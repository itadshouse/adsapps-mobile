package com.production.ads.Activity;


import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.production.ads.App.appController;
import com.production.ads.R;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ActivityMap extends Activity  {

    public appController mController;
    private TextView info;
    private TextView tvTutup;
    public Intent intennyaSaja ;
    public static String POSITION = "position";
    private String pos;
    private Button btnSubmit;
    public GoogleMap map;
    public List<Marker> markersList;
    public boolean mapReady = false;
    private static final int LOCATION = 1;
    private static final int GPS_SETTINGS = 0x7;

    private LocationRequest mLocationRequest;
    private PendingResult<LocationSettingsResult> result;
    private GoogleApiClient client;

    private GPSTracker gps;
    private Double latDouble = 0.0;
    private Double lotDouble = 0.0;
    private String pointString;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mController = appController.getInstance();


        ActivityMap.this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_map);
        mController = appController.getInstance();

        pointString = "";
        info                = findViewById(R.id.info);
        tvTutup             = findViewById(R.id.tvTutup);
        btnSubmit           = findViewById(R.id.btnSubmit);

        tvTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        intennyaSaja        = getIntent();
        pos                 = intennyaSaja.getStringExtra(POSITION);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pointString.equals("")) {
                    Intent intent=new Intent();
                    intent.putExtra("MESSAGE", pointString);
                    intent.putExtra(POSITION, pos);
                    intent.putExtra(ActivityRadioButton.PERTANYAAN_ID, intennyaSaja.getStringExtra(ActivityRadioButton.PERTANYAAN_ID));
                    setResult(1,intent);
                    finish();
                }
            }
        });


        ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMapAsync(new OnMapReadyCallback() {
                                 @Override
                                 public void onMapReady(GoogleMap googleMap) {
                                     map = googleMap;
                                     markersList = new ArrayList<>();
                                     mapReady = true;
                                     client = new GoogleApiClient.Builder(ActivityMap.this)
                                             .addApi(AppIndex.API)
                                             .addApi(LocationServices.API)
                                             .build();
                                     askForPermission(ACCESS_FINE_LOCATION,LOCATION);

                                     btnSubmit.setClickable(true);
                                     btnSubmit.setText("Selesai");
                                     btnSubmit.setBackgroundColor(getResources().getColor(R.color.mdc_brown));


                                     map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                                         @Override
                                         public void onMapClick(LatLng point) {

                                         }
                                     });
                                 }
                             }
                );




    }

    public void MoveCamera (Double Lat, Double Lot) {
        LatLng pos = new LatLng(Lat, Lot);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(Lat,Lot)).zoom(15).build();
        map.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
            }

            @Override
            public void onCancel() {
            }
        });
    }



    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(ActivityMap.this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityMap.this, permission)) {
                ActivityCompat.requestPermissions(ActivityMap.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(ActivityMap.this, new String[]{permission}, requestCode);
            }
        }
        else {
            if (requestCode == 1) {
                askForGPS();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION :
                askForGPS();
                break;
        }
    }


    private void askForGPS(){
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(client, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(ActivityMap.this, GPS_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });

        try {
            gps = new GPSTracker(this);
            if (gps.canGetLocation) {
                latDouble = gps.getLat();
                lotDouble = gps.getLot();

                MoveCamera(latDouble,lotDouble);

                try {
                    LatLng latLng = new LatLng(
                    latDouble, lotDouble);
                    pointString = latLng.toString();

                    map.clear();
                    map.addMarker(new MarkerOptions().position(latLng).title("Your Position").icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                } catch (Throwable e) {

                }

            }
            else {
                showSettingAlert();
            }

        } catch (Exception e) {
            latDouble = 0.0;
            lotDouble = 0.0;

            Log.d("Kesini","Ya2");

            MoveCamera(latDouble,lotDouble);
        }
    }

    public void showSettingAlert () {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(ActivityMap.this);

        alertDialog.setTitle("GPS Belum Aktif");
        alertDialog.setMessage("Ingin Melakukan Konfigurasi ? ");

        alertDialog.setPositiveButton("Konfigurasi GPS", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent (Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);

            }
        });


        alertDialog.show();

    }


    /**
     * Set up the {@link ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
         //   if (getSupportActionBar()!=null)
               // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }



}