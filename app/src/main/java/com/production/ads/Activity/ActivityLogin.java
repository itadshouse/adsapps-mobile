package com.production.ads.Activity;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.production.ads.App.appConfig;
import com.production.ads.App.appController;
import com.production.ads.Model.ModelPertanyaanRealm;
import com.production.ads.ModelRealm.ModelSurveyR;
import com.production.ads.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ActivityLogin extends AppCompatActivity {

    private static ActivityLogin inst;
    private appController mController;
    private EditText email;
    private EditText password;
    private Button login;
    public Realm realm;
    private Call<ResponseBody> result;



    public static ActivityLogin instance() {
        return inst;
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mController = appController.getInstance();

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(ActivityLogin.this).build();

        try {
            realm = Realm.getInstance(realmConfiguration);
        } catch (RealmMigrationNeededException e){
            try {
                Realm.deleteRealm(realmConfiguration);
                realm = Realm.getInstance(realmConfiguration);
            } catch (Exception ex){
                throw ex;
            }
        }


        email                = (EditText) findViewById(R.id.email);
        password             = (EditText) findViewById(R.id.password);

        login                = (Button) findViewById(R.id.login);






        if (mController.isLoggedIn()) {
            /*
            RealmResults<ModelSurveyR> first = realm.where(ModelSurveyR.class).findAll();
            realm.beginTransaction();
            first.clear();
            realm.commitTransaction();

            RealmResults<ModelPertanyaanRealm> t = realm.where(ModelPertanyaanRealm.class).findAll();
            realm.beginTransaction();
            t.clear();
            realm.commitTransaction();

            RealmResults<ModelSurveyDetailRealm> u = realm.where(ModelSurveyDetailRealm.class).findAll();
            realm.beginTransaction();
            u.clear();
            realm.commitTransaction();

            RealmResults<ModelSurveyRealm> g = realm.where(ModelSurveyRealm.class).findAll();
            realm.beginTransaction();
            g.clear();
            realm.commitTransaction();
            */
            Log.d("JANCOKKK", "HALLLOOOOOOOOOOOOOOO 3");
            Intent intent = new Intent(ActivityLogin.this, ActivitySurvey.class);
            startActivity(intent);
            finish();
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().toString().equals("")) {
                    PlaySnake("Masukkan Email Anda");
                }
                else if (password.getText().toString().equals("")) {
                    PlaySnake("Masukkan Password Anda");
                }
                else {
                    /*
                    if (!email.getText().toString().equals("user1@gmail.com")) {
                        PlaySnake("Email Belum Terdaftar");
                    }
                    else if (!password.getText().toString().equals("123456")) {
                        PlaySnake("Password Anda Salah");
                    }
                    else {
                        Intent intent = new Intent(ActivityLogin.this, ActivitySurvey.class);
                        startActivity(intent);
                        finish();
                    }
                    */
                    login.setEnabled(false);
                    login.setText("Sedang Proses Login");
                    login.setBackgroundColor(getResources().getColor(R.color.half_black));
                    Login  ();

                }
            }
        });

        login.setEnabled(false);
        login.setText("Cek Permission");
        login.setBackgroundColor(getResources().getColor(R.color.half_black));
        GetCamera ();
    }


    private int CEKING = 0;

    private void GetCamera () {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        GetLokasi ();
                    }
                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            CEKING = 0;
                            showSettingsDialog();
                        }
                        else {
                            CEKING = 1;
                            Message("Kami Memerlukan Izin Anda Untuk Mengakses Camera Anda");
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void GetLokasi () {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        GetLokasiCoarse();
                    }
                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            CEKING = 0;
                            showSettingsDialog();
                        }
                        else {
                            CEKING = 2;
                            Message("Kami Memerlukan Izin Anda Untuk Mengakses Lokasi Anda");
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void GetLokasiCoarse () {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        GetWrite ();
                    }
                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            CEKING = 0;
                            showSettingsDialog();
                        }
                        else {
                            CEKING = 3;
                            Message("Kami Memerlukan Izin Anda Untuk Mengakses Lokasi Anda");
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    private void GetWrite () {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        GetRead ();
                    }
                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            CEKING = 0;
                            showSettingsDialog();
                        }
                        else {
                            CEKING = 4;
                            Message("Kami Memerlukan Izin Anda Untuk Mengakses Storage Anda");
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void GetRead () {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        GetNetworkState ();
                    }
                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            CEKING = 0;
                            showSettingsDialog();
                        }
                        else {
                            CEKING = 5;
                            Message("Kami Memerlukan Izin Anda Untuk Mengakses Storage Anda");
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void GetNetworkState () {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_NETWORK_STATE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        login.setEnabled(true);
                        login.setText("Login");
                        login.setBackgroundColor(getResources().getColor(R.color.mdc_brown));
                    }
                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            CEKING = 0;
                            showSettingsDialog();
                        }
                        else {
                            CEKING = 6;
                            Message("Kami Memerlukan Izin Anda Untuk Mengakses Lokasi Anda");
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void Message (final String msg) {
        new AlertDialog.Builder(ActivityLogin.this)
                .setTitle("Informasi")
                .setMessage(msg)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (CEKING == 0) {

                                }
                                else if (CEKING == 1) {
                                    GetCamera ();
                                }
                                else if (CEKING == 2) {
                                    GetLokasi ();
                                }
                                else if (CEKING == 3) {
                                    GetLokasiCoarse ();
                                }
                                else if (CEKING == 4) {
                                    GetWrite ();
                                }
                                else if (CEKING == 5) {
                                    GetRead ();
                                }
                                else if (CEKING == 6) {
                                    GetNetworkState ();
                                }
                            }

                        })
                .show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }


    public interface Login {
        @FormUrlEncoded
        @POST("user/login")
        Call<ResponseBody> postMessage(@FieldMap HashMap<String, String> params);
    }

    private void Login () {

        HashMap<String, String> params = new HashMap<>();
        params.put(appConfig.EMAIL, email.getText().toString());
        params.put(appConfig.PASSWORD, password.getText().toString());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mController.client)
                .build();

        Login apiService = retrofit.create(Login.class);
        result = apiService.postMessage(params);
        result.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                login.setEnabled(true);
                login.setText("Login");
                login.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                PlaySnake("Terjadi Kesalahan. Coba Lagi 2");
            }

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> r) {
                RealmResults<ModelSurveyR> first = realm.where(ModelSurveyR.class).findAll();
                realm.beginTransaction();
                first.clear();
                realm.commitTransaction();
                RealmResults<ModelPertanyaanRealm> t = realm.where(ModelPertanyaanRealm.class).findAll();
                realm.beginTransaction();
                t.clear();
                realm.commitTransaction();
                try {
                    String response = r.body().string();
                    JSONObject jObj = new JSONObject(response);
                    //JSONArray jData = jObj.getJSONArray("question");

                    mController.setUserProfile(appConfig.ID, jObj.getString("id"));
                    mController.setUserProfile(appConfig.EMAIL, jObj.getString("email"));
                    mController.setUserProfile(appConfig.NAME, jObj.getString("name"));
                    mController.setUserProfile(appConfig.TIPE, jObj.getString("tipe"));
                    mController.setUserProfile(appConfig.PHONE, jObj.getString("phone"));


                    mController.setLogin(jObj.getString("email"));
                    Intent intent = new Intent(ActivityLogin.this, ActivitySurvey.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {
                    login.setEnabled(true);
                    login.setText("Login");
                    login.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    PlaySnake("Terjadi Kesalahan. Coba Lagi 1");
                }
            }

        });
    }


    public void PlaySnake (String msg) {
        Snackbar mysnack = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG);

        mysnack.getView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) { }
            @Override
            public void onViewDetachedFromWindow(View v) {
                if (findViewById(android.R.id.content) instanceof FloatingActionButton)
                    findViewById(android.R.id.content).setTranslationY(0);
            }
        });
        mysnack.show();
    }

    /*
    public interface Login {
        @FormUrlEncoded
        @POST("user/login")
        Call<ResponseBody> postMessage(@FieldMap HashMap<String, String> params);
    }

    private void Login () {
        HashMap<String, String> params = new HashMap<>();
        params.put(appConfig.EMAIL, email.getText().toString());
        params.put(appConfig.PASSWORD, password.getText().toString());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mController.client)
                .build();

        Login apiService = retrofit.create(Login.class);
        result = apiService.postMessage(params);
        result.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> r) {

            }

        });
    }
    */

}
