package com.production.ads.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.production.ads.Adapter.AdapterSurvey;
import com.production.ads.App.appConfig;
import com.production.ads.App.appController;
import com.production.ads.Model.ModelPertanyaanRealm;
import com.production.ads.Model.ModelSurvey;
import com.production.ads.ModelRealm.ModelSurveyR;
import com.production.ads.R;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ActivitySurvey extends AppCompatActivity {


    private RecyclerView mRecyclerView;
    private List<ModelSurvey> dataList;
    private List<ModelSurvey> dataList2;
    private AdapterSurvey adapter;
    private appController mController;
    private BottomNavigationView bottomNavigationView;
    public Realm realm;
    public ProgressDialog progress;
    private Toolbar toolbar;
    private ProgressBar loading_progress;
    private boolean sudah = false;
    private Call<ResponseBody> result;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
        mController = appController.getInstance();
        loading_progress =  findViewById(R.id.loading_progress);
        loading_progress.setVisibility(View.GONE);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Toolbar tb = findViewById(R.id.toolBar);
        TextView title = tb.findViewById(R.id.toolbarTitle);
        title.setText("Daftar Survey");


        ImageView back  = tb.findViewById(R.id.back);
        ImageView ref   = tb.findViewById(R.id.ref);

        TextView offline    = tb.findViewById(R.id.offline);

        offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(ActivitySurvey.this, ActivityOffline.class);
                startActivityForResult(t,200);
            }
        });

        back.setVisibility(View.GONE);
        mRecyclerView   = findViewById(R.id.search_result);
        dataList = new ArrayList<>();
        dataList2 = new ArrayList<>();
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ActivitySurvey.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        adapter = new AdapterSurvey(ActivitySurvey.this, dataList, mRecyclerView, mRecyclerView);
        mRecyclerView.setAdapter(adapter);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(ActivitySurvey.this).build();

        try {
            realm = Realm.getInstance(realmConfiguration);
        } catch (RealmMigrationNeededException e){
            try {
                Realm.deleteRealm(realmConfiguration);
                realm = Realm.getInstance(realmConfiguration);
            } catch (Exception ex){
                throw ex;
            }
        }

        getDataRealm(false);

        bottomNavigationView = findViewById(R.id.btm_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){
                    case R.id.action_home :
                        //Toast.makeText(BottomNavigationViewActivity.this, "Home clicked", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.action_star :
                        new AlertDialog.Builder(ActivitySurvey.this)
                                .setTitle(getString(R.string.app_name))
                                .setMessage("Apakah Anda Yakin Untuk Logout ?")
                                .setPositiveButton("Ya",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                mController.DeleteALL();
                                                Intent intent = new Intent(ActivitySurvey.this, ActivityLogin.class);
                                                startActivity(intent);
                                                finish();
                                            }

                                        })
                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        bottomNavigationView.setSelectedItemId(R.id.action_home);
                                    }

                                })
                                .show();

                        break;
                    case R.id.action_money :
                        bottomNavigationView.setSelectedItemId(R.id.action_home);
                        Intent t = new Intent(ActivitySurvey.this, ActivityProfile.class);
                        startActivityForResult(t,200);

                        break;
                }

                return true;
            }
        });

        ref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sudah) {
                    getDataRealm(true);
                }

            }
        });

        Log.d(" STRING", mController.getUserProfile(appConfig.ID));

        sudah = true;
    }



    private void getDataRealm(boolean load) {

        dataList2.clear();
        dataList.clear();

        try {

            int jumlah = 0;
            RealmResults<ModelSurveyR> results = realm.where(ModelSurveyR.class).findAll();
            realm.beginTransaction();
            jumlah = results.size();

            if (results.size() > 0) {
                for (int i = 0; i < results.size(); i++) {
                    ModelSurvey r = new ModelSurvey();
                    r.setId(results.get(i).getId());
                    r.setNamaSurvey(results.get(i).getNamaSurvey());
                    r.setJumlahPertanyaan(results.get(i).getJumlahPertanyaan());
                    r.setKeterangan(results.get(i).getKeterangan());
                    r.setInfo(results.get(i).getInfo());
                    r.setDetail(results.get(i).getDetail());
                    r.setDataTersimpan(results.get(i).getDataTersimpan());
                    r.setDraft(results.get(i).getDraft());
                    r.setHilang(results.get(i).getHilang());
                    dataList2.add(r);
                }
            }
            realm.commitTransaction();

            if (dataList2.size() > 0) {

                for (int i = 0; i < dataList2.size(); i++) {
                    jumlah++;
                    ModelSurvey r = new ModelSurvey();
                    r.setId(dataList2.get(i).getId());
                    r.setNamaSurvey(dataList2.get(i).getNamaSurvey());
                    r.setJumlahPertanyaan(dataList2.get(i).getJumlahPertanyaan());
                    r.setKeterangan(dataList2.get(i).getKeterangan());
                    r.setInfo(dataList2.get(i).getInfo());
                    r.setDetail(dataList2.get(i).getDetail());
                    r.setHilang(dataList2.get(i).getHilang());

                    RealmResults<ModelPertanyaanRealm> rs = realm.where(ModelPertanyaanRealm.class).equalTo("formID", dataList2.get(i).getId()).findAll();
                    realm.beginTransaction();
                    if (rs.size() > 0) {
                        r.setDraft("1");
                    }
                    else {
                        r.setDraft("0");
                    }

                    realm.commitTransaction();


                    r.setDataTersimpan(dataList2.get(i).getDataTersimpan());

                    r.setKeterangan(dataList2.get(i).getKeterangan());
                    if (r.getId().equals(mController.getUserProfile(appConfig.LAST_FORM))) {
                        r.setDataTersimpan(mController.getUserProfile(appConfig.LAST_FORM_JUMLAH));
                        mController.setUserProfile(appConfig.LAST_FORM, "");
                    }
                    else {
                        r.setDataTersimpan(dataList2.get(i).getDataTersimpan());
                    }

                    realm.beginTransaction();
                    ModelSurveyR rR = new ModelSurveyR();
                    rR.setId(dataList2.get(i).getId());
                    rR.setNamaSurvey(dataList2.get(i).getNamaSurvey());
                    rR.setJumlahPertanyaan(dataList2.get(i).getJumlahPertanyaan());
                    rR.setKeterangan(dataList2.get(i).getKeterangan());
                    rR.setInfo(dataList2.get(i).getInfo());
                    rR.setDetail(dataList2.get(i).getDetail());
                    rR.setDraft(r.getDraft());
                    rR.setDataTersimpan(dataList2.get(i).getDataTersimpan());
                    rR.setKeterangan(dataList2.get(i).getKeterangan());
                    rR.setDataTersimpan(r.getDataTersimpan());
                    rR.setHilang(dataList2.get(i).getHilang());
                    realm.copyToRealmOrUpdate(rR);
                    realm.commitTransaction();


                    if (r.getHilang().equals("True")) {

                    }
                    else {
                        dataList.add(r);
                    }


                }
                adapter.notifyDataSetChanged();
            }


            if (jumlah > 0){

            }
            else {
                SurveyList  ();
            }

            if (load) {
                SurveyList  ();
            }

        } catch (Throwable e) {

        }
    }

    private void AddData (ModelSurvey product) {
        try {
            realm.beginTransaction();
            ModelSurveyR r = realm.createObject(ModelSurveyR.class);
            r.setNamaSurvey(product.getNamaSurvey());
            r.setKeterangan(product.getKeterangan());
            r.setInfo(product.getInfo());
            r.setJumlahPertanyaan(product.getJumlahPertanyaan());
            r.setId(product.getId());
            r.setDetail(product.getDetail());
            r.setDataTersimpan(product.getDataTersimpan());
            r.setDraft(product.getDraft());
            r.setHilang(product.getHilang());
            realm.commitTransaction();
            dataList.add(product);
        }
        catch (Exception e) {
            realm.commitTransaction();
        }
    }


    public interface SurveyList {
        @FormUrlEncoded
        @POST("user/form")
        Call<ResponseBody> postMessage(@FieldMap HashMap<String, String> params);
    }

    private void SurveyList() {
        mRecyclerView.setVisibility(View.GONE);
        loading_progress.setVisibility(View.VISIBLE);
        HashMap<String, String> params = new HashMap<>();
        params.put(appConfig.ID, mController.getUserProfile(appConfig.ID));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mController.client)
                .build();

        SurveyList apiService = retrofit.create(SurveyList.class);
        result = apiService.postMessage(params);
        result.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mRecyclerView.setVisibility(View.VISIBLE);
                loading_progress.setVisibility(View.GONE);
                PlaySnake("Terjadi Kesalahan. Coba Lagi 2");
            }

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> r) {
                try {

                    mRecyclerView.setVisibility(View.VISIBLE);
                    dataList.clear();
                    loading_progress.setVisibility(View.GONE);

                    String response = r.body().string();
                    JSONArray jData = new JSONArray(response);

                    RealmResults<ModelSurveyR> first = realm.where(ModelSurveyR.class).findAll();
                    realm.beginTransaction();
                    first.clear();
                    realm.commitTransaction();

                    RealmResults<ModelPertanyaanRealm> t = realm.where(ModelPertanyaanRealm.class).findAll();
                    realm.beginTransaction();
                    t.clear();
                    realm.commitTransaction();



                    for (int i = 0; i < jData.length(); i++) {
                        final ModelSurvey product = new ModelSurvey();
                        JSONObject row = jData.getJSONObject(i);
                        product.setInfo("Null");

                        product.setDraft("0");
                        product.setDataTersimpan(row.getString("saved_form"));
                        product.setKeterangan(row.getString("keterangan"));

                        product.setId(row.getString("id"));
                        product.setJumlahPertanyaan("7");

                        product.setNamaSurvey(row.getString("name"));
                        product.setHilang("false");

                        if (row.getString("detail").equals("null")) {
                            product.setDetail("");
                        }
                        else {
                            product.setDetail(row.getString("detail"));
                        }


                        AddData(product);

                    }

                    if (dataList.size() > 0) {
                        adapter.notifyDataSetChanged();
                    } else {
                        PlaySnake("Data Kosong");
                    }


                } catch (Exception e) {
                    PlaySnake("Terjadi Kesalahan. Coba Lagi 1");
                }

            }

        });
    }


    public void PlaySnake (String msg) {
        Snackbar mysnack = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG);

        mysnack.getView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) { }
            @Override
            public void onViewDetachedFromWindow(View v) {
                if (findViewById(android.R.id.content) instanceof FloatingActionButton)
                    findViewById(android.R.id.content).setTranslationY(0);
            }
        });
        mysnack.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("requestCode", String.valueOf(requestCode));

        if (resultCode == 2018) {
            bottomNavigationView.setSelectedItemId(R.id.action_home);
        }

        dataList.clear();
        adapter.notifyDataSetChanged();
        getDataRealm(false);
    }




}
