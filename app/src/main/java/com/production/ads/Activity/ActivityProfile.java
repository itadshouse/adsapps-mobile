package com.production.ads.Activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.production.ads.App.appConfig;
import com.production.ads.App.appController;
import com.production.ads.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * A register screen that offers register via email/password.
 */
public class ActivityProfile extends AppCompatActivity {

    private static ActivityProfile inst;
    private appController mController;

    public static ActivityProfile instance() {
        return inst;
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
        mController = appController.getInstance();

        TextView email  = findViewById(R.id.email);
        TextView nama   = findViewById(R.id.name);
        TextView noHP   = findViewById(R.id.noHP);

        email.setText(mController.getUserProfile(appConfig.EMAIL));
        nama.setText(mController.getUserProfile(appConfig.NAME));
        noHP.setText(mController.getUserProfile(appConfig.PHONE));


        Toolbar tb = findViewById(R.id.toolBar);
        setSupportActionBar(tb);
        TextView title = tb.findViewById(R.id.toolbarTitle);
        title.setText("PROFILE");

        ImageView back  = tb.findViewById(R.id.back);
        ImageView ref   = tb.findViewById(R.id.ref);

        ref.setVisibility(View.GONE);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(2018);
                finish();
            }
        });

        TextView offline             = tb.findViewById(R.id.offline);
        offline.setVisibility(View.GONE);

        TextView device             = findViewById(R.id.device);
        TextView android_version    = findViewById(R.id.androidVersion);
        TextView app_version        = findViewById(R.id.appVersion);

        device.setText(Build.MODEL + " - " +Build.DEVICE);      // API Level
        android_version.setText(Build.VERSION.RELEASE);
        app_version.setText("v.2.3");

    }

    @Override
    public void onBackPressed() {
        setResult(2018);
        finish();
        super.onBackPressed();
    }

}
