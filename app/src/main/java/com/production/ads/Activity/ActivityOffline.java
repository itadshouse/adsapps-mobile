package com.production.ads.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.production.ads.Adapter.AdapterOffline;
import com.production.ads.App.appController;
import com.production.ads.Offline.ModelOffline;
import com.production.ads.R;
import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ActivityOffline extends AppCompatActivity {


    private RecyclerView mRecyclerView;
    public List<ModelOffline> dataList;
    private AdapterOffline adapter;
    private appController mController;
    private Realm realm;
    public Intent intent;

    private TextView kosong;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);
        mController = appController.getInstance();

        mRecyclerView       = findViewById(R.id.search_result);
        Toolbar toolbar     = findViewById(R.id.toolbar);
        Toolbar tb          = findViewById(R.id.toolBar);
        TextView title      = tb.findViewById(R.id.toolbarTitle);
        title.setText("OFFLINE LIST DATA");
        setSupportActionBar(toolbar);


        kosong              = findViewById(R.id.kosong);
        ImageView back      = tb.findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        dataList                = new ArrayList<>();
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ActivityOffline.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        adapter = new AdapterOffline(ActivityOffline.this, dataList, mRecyclerView, mRecyclerView);
        mRecyclerView.setAdapter(adapter);

        intent = getIntent();


        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(ActivityOffline.this).build();

        try {
            realm = Realm.getInstance(realmConfiguration);
        } catch (RealmMigrationNeededException e){
            try {
                Realm.deleteRealm(realmConfiguration);
                realm = Realm.getInstance(realmConfiguration);
            } catch (Exception ex){
                throw ex;
            }
        }


        LoadData();
    }


    public void LoadData() {
        kosong.setVisibility(View.VISIBLE);
        try {
            dataList.clear();
            RealmResults<ModelOffline> rs = realm.where(ModelOffline.class).
                    findAll();
            realm.beginTransaction();
            for (int j = 0; j < rs.size(); j++) {
                ModelOffline p= rs.get(j);
                dataList.add(p);
            }
            if (rs.size() > 0) {
                kosong.setVisibility(View.GONE);
            }
            realm.commitTransaction();
            adapter.notifyDataSetChanged();

       } catch (Exception e) {
            realm.commitTransaction();
       }
    }



}
