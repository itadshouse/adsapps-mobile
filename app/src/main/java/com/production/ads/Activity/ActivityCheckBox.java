package com.production.ads.Activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.production.ads.Adapter.AdapterCheckBox;
import com.production.ads.App.appController;
import com.production.ads.Model.ModelCheckBox;
import com.production.ads.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ActivityCheckBox extends Activity  {


    private RecyclerView mRecyclerView;
    private List<ModelCheckBox>     dataList;
    private AdapterCheckBox adapter;

    protected Handler               handler;

    public appController mController;

    private TextView info;
    private TextView tvInfoPertanyaan;
    private TextView tvTutup;

    public Intent intent;
    public static String ANSWER_GROUP = "answer_group";
    public static String POSITION = "position";
    public static String MAX = "max";


    private ArrayList <String> checkedValue = new ArrayList<String>();


    String pos;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCheckBox.this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_checkbox);

        mRecyclerView       = findViewById(R.id.search_result);
        info                =  findViewById(R.id.info);
        tvInfoPertanyaan    = findViewById(R.id.tvInfoPertanyaan);
        tvTutup             =  findViewById(R.id.tvTutup);

        tvTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mController = appController.getInstance();
        handler = new Handler();
        dataList = new ArrayList<>();
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ActivityCheckBox.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        adapter = new AdapterCheckBox(ActivityCheckBox.this, dataList, mRecyclerView, mRecyclerView, false);
        mRecyclerView.setAdapter(adapter);

        intent              = getIntent();
        pos                 = intent.getStringExtra(POSITION);

        try {
            JSONArray jData = new JSONArray(intent.getStringExtra(ANSWER_GROUP));
            for (int i = 0; i < jData.length(); i++) {
                final ModelCheckBox product = new ModelCheckBox();
                String  row = jData.get(i).toString();
                product.setId(String.valueOf(i+1));
                product.setName(row);
                dataList.add(product);
            }
            info.setVisibility(View.GONE);
            adapter.notifyDataSetChanged();
            adapter.setLoaded();

        } catch (JSONException e) {

        }

        Button btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean lanjut = false;

                if (MAX.equals("") || MAX.equals(null) || MAX.equals("0")) {
                    lanjut = true;
                }
                else {
                    try {
                        if (Integer.parseInt(intent.getStringExtra(MAX)) >= checkedValue.size()) {
                            lanjut = true;
                        }
                        else {
                            lanjut = false;
                            Toast.makeText(getApplicationContext(), "Jawaban Terlalu Banyak", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception e) {
                        lanjut = true;
                    }
                }

                if (lanjut) {
                    if (checkedValue.size() == 0) {
                        finish();
                    }
                    else {
                        Intent intent=new Intent();
                        intent.putExtra("MESSAGE", checkedValue.toString());
                        intent.putExtra(POSITION, pos);
                        setResult(1,intent);
                        finish();
                    }

                }

            }
        });

    }

    public void Add (String data) {
        checkedValue.add(data);
    }

    public void Kurang (String data) {
        checkedValue.remove(data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }



}