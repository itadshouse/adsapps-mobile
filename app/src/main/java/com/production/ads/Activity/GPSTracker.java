package com.production.ads.Activity;

/**
 * Created by Eko on 10/18/2015.
 */

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;

public class GPSTracker extends Service implements LocationListener {

    private final Context context;

    public boolean isGPSEnabled = false;
    public boolean isNetworkEnable  = false;
    public boolean canGetLocation = false;

    double lat;
    double lot;

    private static final long  MIN_DISTANCE_CHANGE_FOT_UPDATES = 10;
    private static final long  MIN_TIME_CBW_UPDATES = 1000 * 60 * 1;

    protected LocationManager locationManager;

    Location location;


    public GPSTracker(Context context) {
        this.context = context;
        getLocation();
    }

    public Location getLocation () {
        try {
            locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnable) {

            }
            else {
                this.canGetLocation = true;

                if (isNetworkEnable) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_CBW_UPDATES, MIN_DISTANCE_CHANGE_FOT_UPDATES, this);

                }

                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    if (location != null) {
                        lat = location.getLatitude();
                        lot = location.getLongitude();
                    }
                }
            }

            if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                            MIN_TIME_CBW_UPDATES, MIN_DISTANCE_CHANGE_FOT_UPDATES, this);

                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                        if (location != null) {
                            lat = location.getLatitude();
                            lot = location.getLongitude();
                        }
                    }

                }
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;

    }

    public void stopUsingGPS () {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    public double getLat () {
        if (location != null) {
            lat = location.getLatitude();
        }
        return lat;
    }

    public double getLot () {
        if (location != null) {
            lot = location.getLongitude();
        }
        return lot;
    }

    public boolean canGetLocation () {
        return this.canGetLocation;
    }

    public void showSettingAlert () {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle("GPS Belum Aktif");
        alertDialog.setMessage("Untuk Pendataan, Kami Memerlukan Data Lokasi Anda. Aktifkan GPS ?");

        alertDialog.setPositiveButton("Konfigurasi", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent (Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);

            }
        });

        alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        alertDialog.show();

    }


    @Override
    public void onLocationChanged(Location arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

}