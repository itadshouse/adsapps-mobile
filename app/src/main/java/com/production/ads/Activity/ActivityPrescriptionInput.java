
package com.production.ads.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.graphics.BitmapCompat;
import android.system.ErrnoException;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.production.ads.App.appController;
import com.production.ads.R;
import com.theartofdev.edmodo.cropper.CropImageView;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ActivityPrescriptionInput extends Activity implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnGetCroppedImageCompleteListener {

    //region: Fields and Consts
    private String KEY_IMAGE = "image";
    private String KEY_NAME = "name";
    private static final int DEFAULT_ASPECT_RATIO_VALUES = 20;

    private static final int ROTATE_NINETY_DEGREES = 90;

    private static final String ASPECT_RATIO_X = "ASPECT_RATIO_X";

    private static final String ASPECT_RATIO_Y = "ASPECT_RATIO_Y";

    private static final int ON_TOUCH = 1;

    private CropImageView mCropImageView;

    private int mAspectRatioX = DEFAULT_ASPECT_RATIO_VALUES;

    private int mAspectRatioY = DEFAULT_ASPECT_RATIO_VALUES;

    Bitmap croppedImage;

    private Uri mCropImageUri;

    String id = "";
    String cek = "";

    private appController mController;
    Button con1;
    String image;
    EditText namaResep;
    EditText keteranganResep;
    Button simpanResep;
    ProgressBar proggresSimpanResep;
     Dialog dialog;
    // Saves the state upon rotating the screen/restarting the activity
    @Override
    protected void onSaveInstanceState(@SuppressWarnings("NullableProblems") Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt(ASPECT_RATIO_X, mAspectRatioX);
        bundle.putInt(ASPECT_RATIO_Y, mAspectRatioY);
    }

    // Restores the state upon rotating the screen/restarting the activity
    @Override
    protected void onRestoreInstanceState(@SuppressWarnings("NullableProblems") Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        mAspectRatioX = bundle.getInt(ASPECT_RATIO_X);
        mAspectRatioY = bundle.getInt(ASPECT_RATIO_Y);
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.crooop);
        mController = appController.getInstance();
        // Initialize components of the app
        mCropImageView = findViewById(R.id.CropImageView);
        final SeekBar aspectRatioXSeek = findViewById(R.id.aspectRatioXSeek);
        final SeekBar aspectRatioYSeek = findViewById(R.id.aspectRatioYSeek);
        Spinner showGuidelinesSpin = findViewById(R.id.showGuidelinesSpin);
        final TextView aspectRatioNum = findViewById(R.id.aspectRatioNum);

        // Sets sliders to be disabled until fixedAspectRatio is set
        aspectRatioXSeek.setEnabled(false);
        aspectRatioYSeek.setEnabled(false);

        // Set initial spinner value
        showGuidelinesSpin.setSelection(ON_TOUCH);

        //Sets the rotate button
        final Button rotateButton = findViewById(R.id.Button_rotate);
        rotateButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCropImageView.rotateImage(ROTATE_NINETY_DEGREES);
            }
        });

        // Sets fixedAspectRatio
        ((ToggleButton) findViewById(R.id.fixedAspectRatioToggle)).setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCropImageView.setFixedAspectRatio(isChecked);
                if (isChecked) {
                    aspectRatioXSeek.setEnabled(false);
                    aspectRatioYSeek.setEnabled(false);
                } else {
                    aspectRatioXSeek.setEnabled(false);
                    aspectRatioYSeek.setEnabled(false);
                }
            }
        });

        ((ToggleButton) findViewById(R.id.fixedAspectRatioToggle)).setChecked(false);

        // Sets crop shape
        ((ToggleButton) findViewById(R.id.cropShapeToggle)).setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCropImageView.setCropShape(isChecked ? CropImageView.CropShape.OVAL : CropImageView.CropShape.RECTANGLE);
            }
        });



        // Sets initial aspect ratio to 10/10, for demonstration purposes
        mCropImageView.setAspectRatio(DEFAULT_ASPECT_RATIO_VALUES, DEFAULT_ASPECT_RATIO_VALUES);

        aspectRatioXSeek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar aspectRatioXSeek, int progress, boolean fromUser) {
                mAspectRatioX = Math.max(1, progress);
                mCropImageView.setAspectRatio(mAspectRatioX, mAspectRatioY);
                aspectRatioNum.setText("(" + mAspectRatioX + ", " + mAspectRatioY + ")");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        aspectRatioYSeek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar aspectRatioYSeek, int progress, boolean fromUser) {
                mAspectRatioY = Math.max(1, progress);
                mCropImageView.setAspectRatio(mAspectRatioX, mAspectRatioY);
                aspectRatioNum.setText("(" + mAspectRatioX + ", " + mAspectRatioY + ")");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        // Sets up the Spinner
        showGuidelinesSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        mCropImageView.setGuidelines(CropImageView.Guidelines.OFF);
                        break;
                    case 1:
                        mCropImageView.setGuidelines(CropImageView.Guidelines.ON_TOUCH);
                        break;
                    case 2:
                        mCropImageView.setGuidelines(CropImageView.Guidelines.ON);
                        break;
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        final TextView snapRadiusNum = (TextView) findViewById(R.id.snapRadiusNum);
        ((SeekBar) findViewById(R.id.snapRadiusSeek)).setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mCropImageView.setSnapRadius(progress);
                snapRadiusNum.setText(Integer.toString(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });





        findViewById(R.id.Button_crop).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCropImageView.getCroppedImageAsync(mCropImageView.getCropShape(), 0, 0);
            }
        });

        findViewById(R.id.Button_load).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivityForResult(getPickImageChooserIntent(), 200);
            }
        });



        ((ToggleButton) findViewById(R.id.fixedAspectRatioToggle)).setVisibility(View.GONE);
        findViewById(R.id.Button_load).setVisibility(View.GONE);
        findViewById(R.id.Button_crop).setVisibility(View.GONE);
        snapRadiusNum.setVisibility(View.GONE);
        showGuidelinesSpin.setVisibility(View.GONE);
        aspectRatioYSeek.setVisibility(View.GONE);
        aspectRatioXSeek.setVisibility(View.GONE);
        ((ToggleButton) findViewById(R.id.cropShapeToggle)).setVisibility(View.GONE);
        ((ToggleButton) findViewById(R.id.fixedAspectRatioToggle)).setVisibility(View.GONE);
        rotateButton.setVisibility(View.GONE);
        aspectRatioNum.setVisibility(View.GONE);
        ((SeekBar) findViewById(R.id.snapRadiusSeek)).setVisibility(View.GONE);



        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            Uri myUri = Uri.parse(extras.getString("imageUri"));

            Bitmap bitmap = decodeUriToBitmap(ActivityPrescriptionInput.this, myUri);

            Log.d("IMAGE_SIZE", "HALLLOOOOOOOOOOOOOOO 1");


            image = getStringImage(bitmap);
            Intent intent=new Intent();
            intent.putExtra("image", image);
            setResult(201,intent);
            finish();


            /*
            id = extras.getString("id");
            cek = extras.getString("cek");


            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&

                    isUriRequiresPermissions(myUri)) {

                // request permissions and handle the result in onRequestPermissionsResult()
                requirePermissions = true;
                mCropImageUri = myUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                ((CropImageView) findViewById(R.id.CropImageView)).setImageUriAsync(myUri);
            }
            */
        }

        con1 = findViewById(R.id.con);


        con1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCropImageView.getCroppedImageAsync(mCropImageView.getCropShape(), 0, 0);
            }
        });


    }


    public static Bitmap decodeUriToBitmap (Context mContext, Uri sendUri) {
        Bitmap getBitmap = null;
        try {
            getBitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(),sendUri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getBitmap;
    }



    @Override
    protected void onStart() {
        super.onStart();
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnGetCroppedImageCompleteListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCropImageView.setOnSetImageUriCompleteListener(null);
        mCropImageView.setOnGetCroppedImageCompleteListener(null);
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
            //Toast.makeText(this, "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            //Log.e("AIC", "Failed to load image by URI", error);
           // Toast.makeText(this, "Image load failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }




    @Override
    public void onGetCroppedImageComplete(CropImageView view, Bitmap bitmap, Exception error) {
        if (error == null) {
            image = getStringImage(bitmap);
            Intent intent=new Intent();
            intent.putExtra("image", image);
            setResult(201,intent);
            finish();
        } else {
            Log.e("AIC", "Failed to crop image", error);
            Toast.makeText(this, "Image crop failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("nama_toko","close");
        setResult(2, intent);
        finish();//finishing activity
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            ((CropImageView) findViewById(R.id.CropImageView)).setImageUriAsync(mCropImageUri);
        } else {
            Toast.makeText(this, "Required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Create a chooser intent to select the source to get image from.<br/>
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the intent chooser.
     */
    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }


    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    /**
     * Test if we can open the given Android URI to test if permission required error is thrown.<br>
     */
    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }



    public String getStringImage(Bitmap bmp){
        int bitmapByteCount = BitmapCompat.getAllocationByteCount(bmp);
        Log.d("IMAGE_SIZE", "DALAM BYTE : " + bitmapByteCount);


        try {
            if (bmp.getWidth() == bmp.getHeight()) {
                if (bmp.getHeight() > 500) {
                    bmp =  bmp.createScaledBitmap(bmp, 500, 500, true);
                }
            }
            else if (bmp.getHeight() > bmp.getWidth()) {
                if (bmp.getHeight() > 500) {

                    int y = bmp.getHeight();
                    int x = bmp.getWidth();

                    int j = 500;
                    int k = (j * x) / y;


                    bmp =  bmp.createScaledBitmap(bmp, k, j, true);

                }


            }
            else if (bmp.getWidth() > bmp.getHeight()) {


                if (bmp.getWidth() > 500) {
                    int y = bmp.getWidth();
                    int x = bmp.getHeight();


                    int j = 500;
                    int k = (j * x) / y;


                    bmp =  bmp.createScaledBitmap(bmp, j, k, true);
                }

            }

            bitmapByteCount = BitmapCompat.getAllocationByteCount(bmp);
            Answers.getInstance().logCustom(new CustomEvent("SUCCESS RESIZE IMAGE")
                    .putCustomAttribute("IMAGE_SIZE", String.valueOf(bitmapByteCount))
                    .putCustomAttribute("PHONE", Build.MODEL + " - " +Build.DEVICE)
                    .putCustomAttribute("OS", Build.VERSION.RELEASE));


        }
        catch (Exception e){
            bitmapByteCount = BitmapCompat.getAllocationByteCount(bmp);
            Answers.getInstance().logCustom(new CustomEvent("ERROR RESIZE IMAGE")
                    .putCustomAttribute("IMAGE_SIZE", String.valueOf(bitmapByteCount))
                    .putCustomAttribute("PHONE", Build.MODEL + " - " +Build.DEVICE)
                    .putCustomAttribute("OS", Build.VERSION.RELEASE));
        }


        bitmapByteCount = BitmapCompat.getAllocationByteCount(bmp);


        Log.d("IMAGE_SIZE", "DALAM BYTE : " + bitmapByteCount);



        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 60, baos);


        bitmapByteCount = BitmapCompat.getAllocationByteCount(bmp);
        Log.d("IMAGE_SIZE", "DALAM BYTE 2 : " + bitmapByteCount);


        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.NO_WRAP);
        return encodedImage;
    }


}
