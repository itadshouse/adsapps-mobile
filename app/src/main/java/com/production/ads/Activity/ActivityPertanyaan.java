package com.production.ads.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.production.ads.Adapter.AdapterPertanyaan;
import com.production.ads.App.appConfig;
import com.production.ads.App.appController;
import com.production.ads.Model.ModelPertanyaan;
import com.production.ads.Model.ModelPertanyaanRealm;
import com.production.ads.ModelRealm.ModelSurveyR;
import com.production.ads.Offline.ModelOffline;
import com.production.ads.Offline.ModelOfflineDetail;
import com.production.ads.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;

import static android.Manifest.permission.READ_PHONE_STATE;

public class ActivityPertanyaan extends AppCompatActivity {

    private static final int PHONE_STATE_INT = 2018;
    private RecyclerView mRecyclerView;
    public List<ModelPertanyaan> dataList;
    private AdapterPertanyaan adapter;
    private appController mController;

    private int REQUEST_CAMERA = 3, SELECT_FILE = 4;
    private String userChoosenTask;

    private int complete = 0;
    private TextView tvComplete;
    public Intent intent;

    public String[] pertanyaanID;
    public String[] jawaban;
    private ProgressDialog progress;

    private boolean belumBolehSubmit = true;
    private int jumlah = 0;

   // Button btnSubmit;

    private static final int GPS_SETTINGS = 0x7;

    LocationRequest mLocationRequest;
    PendingResult<LocationSettingsResult> result;
    GoogleApiClient client;


    private GPSTracker gps;
    private Double latDouble = 0.0;
    private Double lotDouble = 0.0;

    public String PertanyaanID = "";
    public String POSITION;
    public String MAX = "";
    public String JENIS = "";
    public String TIPE_TULISAN = "";


    private static final int LOCATION = 1;
    private static final int READ_EXST = 2;
    private static final int CAMERA_EXST = 3;



    //public RelativeLayout LayoutSubmit;
    public LinearLayout LayoutJawaban;
    public RelativeLayout LayoutTransparant;

    public EditText tvEmail;
    public EditText tvJawaban;
    public EditText tvNumber;
    public EditText tvNote;
    public EditText tvLower;
    public EditText tvFloat;

    public TextView tvTutup;
    public Button buttonSelesai;
    public boolean isChild = false;
    public String[] pertanyaanIDChild;
    public String[] jawabanChild;
    public int i = 0;
    private Realm realm;
    private Toolbar toolbar;

    private Call<ResponseBody> resOK;
    public List<ModelPertanyaan> dataListSesungguhnya;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private boolean LagiSubmit = false;

    @Override
    public void onBackPressed() {
       if (!LagiSubmit) {
           super.onBackPressed();
           finish();
       }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pertanyaan);
        mController = appController.getInstance();
        mRecyclerView   = (RecyclerView) findViewById(R.id.search_result);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Toolbar tb = (Toolbar)findViewById(R.id.toolBar);
        TextView title = (TextView) tb.findViewById(R.id.toolbarTitle);
        title.setText("");

        ImageView back  = (ImageView) tb.findViewById(R.id.back);
        ImageView ref   = (ImageView) tb.findViewById(R.id.ref);

        TextView offline    = tb.findViewById(R.id.offline);
        offline.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        dataList                = new ArrayList<>();
        dataListSesungguhnya    = new ArrayList<>();

        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ActivityPertanyaan.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        adapter = new AdapterPertanyaan(ActivityPertanyaan.this, dataListSesungguhnya, mRecyclerView, mRecyclerView);
        mRecyclerView.setAdapter(adapter);


        LayoutJawaban       = findViewById(R.id.LayoutJawaban);
        //LayoutSubmit        = findViewById(R.id.LayoutSubmit);
        LayoutTransparant   = findViewById(R.id.LayoutTransparant);
        tvNumber            = findViewById(R.id.tvNumber);
        tvEmail             = findViewById(R.id.tvEmail);
        tvJawaban           = findViewById(R.id.tvJawaban);
        tvNote              = findViewById(R.id.tvNote);
        tvLower             = findViewById(R.id.tvLower);
        tvFloat             = findViewById(R.id.tvFloat);
        tvTutup             = findViewById(R.id.tvTutup);
        buttonSelesai       = findViewById(R.id.buttonSelesai);
        //btnSubmit           = findViewById(R.id.btnSubmit);

        LayoutJawaban.setVisibility(View.GONE);

        buttonSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvJawaban.getText().toString().equals("")) {
                    Menjawab(tvJawaban);
                }
                else if (!tvEmail.getText().toString().equals("")) {
                    if (isValidEmail(tvEmail.getText().toString())) {
                        Menjawab(tvEmail);
                    }
                    else {
                        PlaySnake("Email Tidak Valid");
                    }
                }
                else if (!tvNumber.getText().toString().equals("")) {
                    Menjawab(tvNumber);
                }
                else if (!tvNote.getText().toString().equals("")) {
                    Menjawab(tvNote);
                }
                else if (!tvLower.getText().toString().equals("")) {
                    Menjawab(tvLower);
                }
                else if (!tvFloat.getText().toString().equals("")) {
                    Menjawab(tvFloat);
                }
                else {
                    PlaySnake("Isi Jawaban Terlebih Dahulu");
                }
            }
        });

        tvTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
                LayoutTransparant.setVisibility(View.GONE);
                LayoutJawaban.setVisibility(View.GONE);
               // LayoutSubmit.setVisibility(View.VISIBLE);
            }
        });


        tvComplete = (TextView) findViewById(R.id.tvComplete);
        tvComplete.setText("Complete " + String.valueOf(0) + "/" + "0");

        intent = getIntent();


        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(ActivityPertanyaan.this).build();

        try {
            realm = Realm.getInstance(realmConfiguration);
        } catch (RealmMigrationNeededException e){
            try {
                Realm.deleteRealm(realmConfiguration);
                realm = Realm.getInstance(realmConfiguration);
            } catch (Exception ex){
                throw ex;
            }
        }





        pertanyaanID    = new String[500];
        jawaban         = new String[500];

        pertanyaanIDChild    = new String[500];
        jawabanChild         = new String[500];


        /*
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        */


        client = new GoogleApiClient.Builder(this)
                .addApi(AppIndex.API)
                .addApi(LocationServices.API)
                .build();

        ref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RealmResults<ModelPertanyaanRealm> rs = realm.where(ModelPertanyaanRealm.class).equalTo("formID", intent.getStringExtra(appConfig.FORM_ID)).findAll();
                realm.beginTransaction();
                rs.clear();
                realm.commitTransaction();

                dataListSesungguhnya.clear();
                dataList.clear();
                adapter.notifyDataSetChanged();
                LoadData();




                new AlertDialog.Builder(ActivityPertanyaan.this)
                        .setTitle(getString(R.string.app_name))
                        .setMessage("Data Berhasil Diperbaharui")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }

                                })
                        .show();
            }
        });



        LoadData();
    }


    public void GoSubmit (Button btnSubmit) {
        btnSubmit.setEnabled(false);
        if (!belumBolehSubmit) {

            if (jumlah == 0) {
                finish();
            }
            else {
                boolean submit = true;
                String numberNya = "";
                boolean sudahDidefinisikan = false;

                for (int i = 0; i < dataList.size(); i++) {
                    if (dataList.get(i).getMandatory().equals("*")
                            && !dataList.get(i).getQuestion_type().equals("Section")
                            && dataList.get(i).isManda()) {
                        if (dataList.get(i).getJawaban().equals("")) {

                            if (!sudahDidefinisikan) {
                                numberNya = dataList.get(i).getPosition();
                                sudahDidefinisikan = true;
                            }
                            submit = false;
                        }
                    }
                }

                if (submit) {
                    askForPermission(ACCESS_FINE_LOCATION,LOCATION);
                }
                else {
                    PlaySnake("Isi Jawaban Dari Pertanyaan No. " + numberNya);
                    btnSubmit.setEnabled(true);
                }
            }
        }
        else {
            PlaySnake("Tunggu Hingga Semua Pertanyaan Berhasil Diload");
            btnSubmit.setEnabled(true);
        }

    }


    private void LoadData() {
        try {
            dataList.clear();
            belumBolehSubmit = false;
            JSONArray jData = new JSONArray(intent.getStringExtra("DETAIL"));

            //btnSubmit.setEnabled(true);

            int nomor = 0;

            for (int n = 0; n < jData.length(); n++) {
                final ModelPertanyaan product = new ModelPertanyaan();
                JSONObject  row = jData.getJSONObject(n);
                JSONObject  tipe = row.getJSONObject("tipe");


                if (row.getString("ismultiple").equals("Y")) {
                    product.setMultiple(true);
                }
                else {
                    product.setMultiple(false);
                }

                product.setPosition(row.getString("position"));
                product.setId(row.getString("namainputan"));
                product.setDestination("null");
 
                RealmResults<ModelPertanyaanRealm> results = realm.where(ModelPertanyaanRealm.class).equalTo("realID", row.getString("namainputan") + "FORM" + intent.getStringExtra(appConfig.FORM_ID)).findAll();


                if (results.size() > 0) {

                    product.setJawaban(results.get(0).getJawaban());

                    if (tipe.getString("name").toString().equals("Image") || tipe.getString("name").toString().equals("Gallery") ) {
                        try {
                            File getImage = (ActivityPertanyaan.this).getExternalCacheDir();
                            File destination = new File(getImage.getPath(), results.get(0).getDestination());
                            product.setDestination(destination.toString());

                            pertanyaanID[i] = results.get(0).getId();
                            jawaban[i]      = results.get(0).getJawaban();
                            i++;
                            complete++;
                            //Log.d("COMPLETE", "Complete " + String.valueOf(complete) + "/" + String.valueOf(jumlah));
                            tvComplete.setText("Complete " + String.valueOf(complete) + "/" + String.valueOf(jumlah));
                        }
                        catch (Exception e) {
                            realm.beginTransaction();
                            results.clear();
                            realm.commitTransaction();
                        }
                    }
                    else {
                        pertanyaanID[i] = results.get(0).getId();
                        jawaban[i]      = results.get(0).getJawaban();
                        i++;
                    }
                }
                else {
                    product.setJawaban("");
                }


                product.setPertanyaan(row.getString("label"));
                product.setUrutan("");
                product.setImage(null);
                product.setAnswer_group(row.getString("option"));
                product.setQuestion_type(tipe.getString("name"));
                product.setMaximum(row.getString("maximum"));
                product.setTipeTulisan(row.getString("tipetulisan"));

                if (!tipe.getString("name").equals("Section")) {
                    nomor++;
                    product.setNomor(String.valueOf(nomor));
                }
                else {
                    product.setNomor("0");
                }

                product.setKondisi(row.getString("kondisi"));

                if (row.getString("desc").equals("null")) {
                    product.setDeskripsi("");
                }
                else {
                    product.setDeskripsi(row.getString("desc"));
                }


                if (!row.getString("kondisi").equals("null")) {
                 
                    JSONArray detailKondisi = new JSONArray(row.getString("detail_kondisi"));

                    for (int k = 0; k < detailKondisi.length(); k++) {
                        JSONObject  r = detailKondisi.getJSONObject(k);
                        if (k == 0) {
                            product.setDetail_kondisi_id(r.getString("pertanyaanID"));
                            product.setDetail_kondisi_notasi(r.getString("notasi"));
                            product.setDetail_kondisi_nilai(r.getString("nilai"));
                        }
                        else {
                            product.setDetail_kondisi_id_2(r.getString("pertanyaanID"));
                            product.setDetail_kondisi_notas_2(r.getString("notasi"));
                            product.setDetail_kondisi_nilai_2(r.getString("nilai"));
                        }
                    }
                }
                else {
                    product.setDetail_kondisi_id("");
                    product.setDetail_kondisi_notasi("");
                    product.setDetail_kondisi_id_2("");
                    product.setDetail_kondisi_notas_2("");
                    product.setDetail_kondisi_nilai("");
                    product.setDetail_kondisi_nilai_2("");
                }

                product.setMandatory("");

                if (tipe.getString("name").equals("Group")) {
                    product.setChild(row.getString("child"));

                    JSONArray jDataChild = new JSONArray(row.getString("child"));

                    for (int ads = 0; ads < jDataChild.length(); ads++) {
                        JSONObject rowChild = jDataChild.getJSONObject(ads);

                        if (rowChild.getString("mandatory").equals("Y")) {
                            product.setMandatory("*");
                        }
                    }

                }
                else {
                    product.setChild("");

                    if (row.getString("mandatory").equals("Y")) {
                        product.setMandatory("*");
                    }
                    else {
                        product.setMandatory("");
                    }

                }
                dataList.add(product);
                tvComplete.setText("Complete " + String.valueOf(complete) + "/" + nomor  );
                jumlah = nomor;
            }


            for (int n = 0; n < dataList.size(); n++) {
                ModelPertanyaan product = dataList.get(n);

                boolean hilang;

                if (product.getKondisi().toString().equals("null") ) {
                    hilang = false;
                }
                else {
                    hilang = true;
                    String id_kondisi = product.getDetail_kondisi_id();
                    if (product.getDetail_kondisi_id() != null && product.getDetail_kondisi_id_2() != null) {
                        String jawaban = "";


                        for (int senin = 0; senin < dataList.size(); senin++) {

                            if (product.getDetail_kondisi_id().equals(dataList.get(senin).getId())) {

                                if (dataList.get(senin).getJawaban().equals("")) {
                                    hilang = true;
                                }
                                else {
                                    jawaban = dataList.get(senin).getJawaban();
                                }
                            }
                        }

                        String jawaban2 = "";

                        for (int senin = 0; senin < dataList.size(); senin++) {
                            if (product.getDetail_kondisi_id_2().equals(dataList.get(senin).getId())) {

                                if (dataList.get(senin).getJawaban().equals("")) {
                                    hilang = true;
                                }
                                else {
                                    jawaban2 = dataList.get(senin).getJawaban();
                                }
                            }
                        }
                        if (product.getKondisi().equals("AND") && !jawaban.equals("") && !jawaban2.equals("")) {
                            boolean one = MapString.opByName.get(product.getDetail_kondisi_notasi()).calculate(jawaban,
                                    product.getDetail_kondisi_nilai());

                            boolean two = MapString.opByName.get(product.getDetail_kondisi_notas_2()).calculate(jawaban2,
                                    product.getDetail_kondisi_nilai_2());

                            if (one && two) {
                                hilang = false;
                            }
                        }
                        else if (product.getKondisi().equals("OR") && !jawaban.equals("") && !jawaban2.equals("")) {
                            boolean one = MapString.opByName.get(product.getDetail_kondisi_notasi()).calculate(jawaban,
                                    product.getDetail_kondisi_nilai());

                            boolean two = MapString.opByName.get(product.getDetail_kondisi_notas_2()).calculate(jawaban2,
                                    product.getDetail_kondisi_nilai_2());

                            if (one || two) {
                                hilang = false;
                            }
                        }
                        else {
                            hilang = true;
                        }
                    }
                    else if (product.getDetail_kondisi_id() != null) {
                        String jawaban = "";

                        for (int senin = 0; senin < dataList.size(); senin++) {
                            if (id_kondisi.equals(dataList.get(senin).getId())) {

                                if (dataList.get(senin).getJawaban().equals("")) {
                                    hilang = true;
                                }
                                else {
                                    jawaban = dataList.get(senin).getJawaban();
                                }
                            }
                        }

                        if (!jawaban.equals("")) {
                            boolean one = MapString.opByName.get(product.getDetail_kondisi_notasi()).calculate(jawaban,
                                    product.getDetail_kondisi_nilai());

                            if (one) {
                                hilang = false;
                            }
                        }
                        else {
                            hilang = true;

                        }
                    }
                }
                if (hilang) {
                    RealmResults<ModelPertanyaanRealm> results = realm.where(ModelPertanyaanRealm.class).equalTo("realID", product.getId() + "FORM" + intent.getStringExtra(appConfig.FORM_ID)).findAll();
                    if (results.size() > 0) {
                        realm.beginTransaction();
                        results.clear();
                        realm.commitTransaction();
                    }
                }
                else {
                    dataListSesungguhnya.add(product);
                }
            }

            final ModelPertanyaan product = new ModelPertanyaan();
            product.setQuestion_type("SUBMIT");
            dataListSesungguhnya.add(product);


            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
        }
    }

    public void Menjawab (EditText editText) {

        boolean lanjut = true;
        Log.d("MAXIMAL", " : " + MAX);

        if (MAX.equals("") || MAX.equals(null) || MAX.equals("0")) {
            lanjut = true;
        }
        else {
            if (JENIS.equals("")) {
                lanjut = true;
            }
            else {

                try {
                    if (Float.parseFloat(editText.getText().toString()) <= Integer.parseInt(MAX)) {
                        lanjut = true;
                    }
                    else {
                        lanjut = false;
                        Toast.makeText(ActivityPertanyaan.this, "Jawaban Terlalu Panjang", Toast.LENGTH_SHORT).show();
                    }
                }

                catch (Exception e) {
                    try {
                        if (editText.length() <= Integer.parseInt(MAX)) {
                            lanjut = true;
                        }
                        else {
                            lanjut = false;
                            Toast.makeText(ActivityPertanyaan.this, "Jawaban Terlalu Panjang", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception B) {
                        lanjut = true;
                    }
                }


            }
        }

        if (lanjut) {
            if (!isChild) {
                if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {
                    dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                    pertanyaanID[i] = PertanyaanID;
                    jawaban[i]      = editText.getText().toString();
                    i++;
                    complete++;
                }
                else {

                    pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = PertanyaanID;
                    jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = editText.getText().toString();
                }
            }
            else {

                if (adapter.productView.dataList.get(0).getUrutan().equals("")) {
                    adapter.productView.dataList.get(0).setUrutan(String.valueOf(i));
                    pertanyaanID[i] = PertanyaanID;
                    jawaban[i]      = editText.getText().toString();
                    i++;
                    complete++;
                }
                else {
                    pertanyaanID[Integer.parseInt(adapter.productView.dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = PertanyaanID;
                    jawaban[Integer.parseInt(adapter.productView.dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = editText.getText().toString();
                }
            }




            Up(editText.getText().toString());
        }
    }

    public void Up (String jawaban) {

        realm.beginTransaction();
        ModelPertanyaanRealm model = new ModelPertanyaanRealm();
        model.setRealID(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
        model.setId(PertanyaanID);
        model.setFormID(intent.getStringExtra(appConfig.FORM_ID));
        model.setJawaban(jawaban);
        realm.copyToRealmOrUpdate(model);
        realm.commitTransaction();


        if (!isChild) {

            dataListSesungguhnya.clear();
            dataList.clear();
            adapter.notifyDataSetChanged();
            LoadData();

            //dataList.get(Integer.parseInt(POSITION)).setJawaban(jawaban.replaceAll("\\[", "").replaceAll("\\]",""));
            //adapter.notifyDataSetChanged();
        }
        else {
           // adapter.productView.dataList.get(0).setJawaban(jawaban.replaceAll("\\[", "").replaceAll("\\]",""));
           // adapter.productView.adapter.notifyDataSetChanged();

            dataListSesungguhnya.clear();
            dataList.clear();
            adapter.notifyDataSetChanged();
            LoadData();


        }

        tvComplete.setText("Complete " + String.valueOf(complete) + "/" + String.valueOf(jumlah));
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
        LayoutTransparant.setVisibility(View.GONE);
        LayoutJawaban.setVisibility(View.GONE);
        //LayoutSubmit.setVisibility(View.VISIBLE);
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }



    private void askForGPS(){
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(client, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(ActivityPertanyaan.this, GPS_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });

        try {
            gps = new GPSTracker(this);
            if (gps.canGetLocation) {
                latDouble = gps.getLat();
                lotDouble = gps.getLot();
                askForPermission(READ_PHONE_STATE,PHONE_STATE_INT);
            }
            else {
                showSettingAlert();
            }
        } catch (Exception e) {
            latDouble = 0.0;
            lotDouble = 0.0;
            askForPermission(READ_PHONE_STATE,PHONE_STATE_INT);
        }
    }

    public void showSettingAlert () {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(ActivityPertanyaan.this);

        alertDialog.setTitle("GPS Belum Aktif");
        alertDialog.setMessage("Untuk pendataan, kami memerlukan data lokasi anda");

        alertDialog.setPositiveButton("Konfigurasi GPS", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent (Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);

            }
        });

        alertDialog.setNegativeButton("Lanjtukan Tanpa Data Lokasi", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                latDouble = 0.0;
                lotDouble = 0.0;
                askForPermission(READ_PHONE_STATE,PHONE_STATE_INT);
            }
        });

        alertDialog.show();

    }

    String status = "";
    String saved_form = "";
    int theIndex = 0;


    public interface Send {
        @FormUrlEncoded
        @POST("user/form/save")
        Call<ResponseBody> postMessage(@FieldMap HashMap<String, String> params);
    }

    private void Send (final String imei) {

        LagiSubmit = true;
        theIndex = 0;

        final long time = System.currentTimeMillis();

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        final String waktu = dateFormat.format(cal.getTime());

        progress = new ProgressDialog(this);
        progress.setMessage("Sedang Mengirim ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.show();


        HashMap<String, String> params = new HashMap<>();
        params.put("userID", mController.getUserProfile(appConfig.ID));
        params.put("formID", intent.getStringExtra(appConfig.FORM_ID));
        params.put("timeStamp", waktu);
        params.put("latitude", String.valueOf(latDouble));
        params.put("longtitude", String.valueOf(lotDouble));
        params.put("IMEI", imei);
        params.put("IMEI_MD5", md5(imei+String.valueOf(time)));


        RealmResults<ModelPertanyaanRealm> rs = realm.where(ModelPertanyaanRealm.class).
                equalTo("formID", intent.getStringExtra(appConfig.FORM_ID)).
                equalTo("idChild","CHILD").
                findAll();
        realm.beginTransaction();
        for (int j = 0; j < rs.size(); j++) {
            pertanyaanIDChild[theIndex] = rs.get(j).getId();
            jawabanChild[theIndex]      = rs.get(j).getJawaban();
            theIndex++;
        }
        realm.commitTransaction();


        int x = 0;
        for (int j = 0; j < i; j++) {
            params.put("pertanyaanId[" + String.valueOf(j) + "]", pertanyaanID[j]);
            params.put("jawaban[" + String.valueOf(j) + "]", jawaban[j]);


            Log.d("pertanyaanId[" + String.valueOf(j) + "]", pertanyaanID[j]);
            Log.d("jawaban[" + String.valueOf(j) + "]", jawaban[j]);


            x = j;
        }

        for (int j = 0; j < theIndex; j++) {
            x++;
            params.put("pertanyaanId[" + String.valueOf(x) + "]", pertanyaanIDChild[j]);
            params.put("jawaban[" + String.valueOf(x) + "]", jawabanChild[j]);

            Log.d("pertanyaanId[" + String.valueOf(x) + "]", pertanyaanIDChild[j]);
            Log.d("jawaban[" + String.valueOf(x) + "]", jawabanChild[j]);

        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mController.client)
                .build();

        Send apiService = retrofit.create(Send.class);
        resOK = apiService.postMessage(params);
        resOK.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                GagalKirim(waktu,imei,time);
            }

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> r) {


                Log.d("Gagal Kirim", "OK 1");


                LagiSubmit = false;
                status = "";
                saved_form = "";

                try {
                    String response = r.body().string();
                    JSONObject jData = new  JSONObject(response);
                    status = jData.getString("status");
                    saved_form = jData.getString("saved_form");
                    progress.dismiss();

                    mController.setUserProfile(appConfig.LAST_FORM, intent.getStringExtra(appConfig.FORM_ID));
                    mController.setUserProfile(appConfig.LAST_FORM_JUMLAH, saved_form);

                    RealmResults<ModelSurveyR> s = realm.where(ModelSurveyR.class).equalTo("id", intent.getStringExtra(appConfig.FORM_ID)).findAll();
                    ModelSurveyR rR = new ModelSurveyR();
                    realm.beginTransaction();
                    rR.setId(s.get(0).getId());
                    rR.setNamaSurvey(s.get(0).getNamaSurvey());
                    rR.setJumlahPertanyaan(s.get(0).getJumlahPertanyaan());
                    rR.setKeterangan(s.get(0).getKeterangan());
                    rR.setInfo(s.get(0).getInfo());
                    rR.setDetail(s.get(0).getDetail());
                    rR.setDraft(s.get(0).getDraft());
                    rR.setDataTersimpan(s.get(0).getDataTersimpan());
                    rR.setKeterangan(s.get(0).getKeterangan());
                    rR.setDataTersimpan(s.get(0).getDataTersimpan());


                    if (status.equals("Anda tidak memiliki akses..")) {
                        rR.setHilang("True");
                    }
                    else {
                        rR.setHilang(s.get(0).getHilang());
                    }

                    realm.copyToRealmOrUpdate(rR);
                    realm.commitTransaction();

                    RealmResults<ModelPertanyaanRealm> rs = realm.where(ModelPertanyaanRealm.class).equalTo("formID", intent.getStringExtra(appConfig.FORM_ID)).findAll();
                    realm.beginTransaction();
                    rs.clear();
                    realm.commitTransaction();

                    new AlertDialog.Builder(ActivityPertanyaan.this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Submit Keterangan")
                            .setMessage(status)
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(ActivityPertanyaan.this, ActivitySurvey.class);
                                    startActivity(intent);
                                    finish();

                                }
                            })
                            .show();
                } catch (Exception e) {
                    GagalKirim(waktu,imei,time);
                }
            }

        });
    }

    private void GagalKirim (String waktu, String imei, long time) {
        try {
            realm.beginTransaction();
            ModelOffline r = realm.createObject(ModelOffline.class);
            r.setCreatedAt(waktu);
            r.setUserID(mController.getUserProfile(appConfig.ID));
            r.setFormID(intent.getStringExtra(appConfig.FORM_ID));
            r.setFormName(intent.getStringExtra("FORM_NAME"));
            r.setTimeStamp(waktu);
            r.setLatitude(String.valueOf(latDouble));
            r.setLongtitude(String.valueOf(lotDouble));
            r.setIMEI(imei);
            r.setIMEI_MD5(md5(imei+String.valueOf(time)));
            realm.commitTransaction();
        }
        catch (Exception e) {
            realm.commitTransaction();
        }

        try {
            for (int j = 0; j < i; j++) {
                realm.beginTransaction();
                ModelOfflineDetail r = realm.createObject(ModelOfflineDetail.class);
                r.setCreatedAt(waktu);
                r.setPertanyaanID(pertanyaanID[j]);
                r.setJawaban(jawaban[j]);
                r.setIndex(String.valueOf(j));
                realm.commitTransaction();
            }
        }
        catch (Exception e) {
            realm.commitTransaction();
        }

        try {
            for (int j = 0; j < theIndex; j++) {
                realm.beginTransaction();
                ModelOfflineDetail r = realm.createObject(ModelOfflineDetail.class);
                r.setCreatedAt(waktu);
                r.setPertanyaanID(pertanyaanIDChild[j]);
                r.setJawaban(jawabanChild[j]);
                r.setIndex(String.valueOf(j));
                realm.commitTransaction();
            }
        }
        catch (Exception e) {
            realm.commitTransaction();
        }

        RealmResults<ModelPertanyaanRealm> rs = realm.where(ModelPertanyaanRealm.class).equalTo("formID", intent.getStringExtra(appConfig.FORM_ID)).findAll();
        realm.beginTransaction();
        rs.clear();
        realm.commitTransaction();

        try {
            LagiSubmit = false;
            progress.dismiss();
            new AlertDialog.Builder(ActivityPertanyaan.this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle("Submit")
                    .setMessage("Data Gagal Terkirim. Data Dipindahkan ke Offline")
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent(ActivityPertanyaan.this, ActivitySurvey.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .show();
        }
        catch (Exception e) {

        }
    }


    public String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }

            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==200 && resultCode  ==Activity.RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);
            Intent intent = new Intent(ActivityPertanyaan.this, ActivityPrescriptionInput.class);
            intent.putExtra("id", "");
            intent.putExtra("imageUri", imageUri.toString());
            intent.putExtra("cek", "false");
            ActivityPertanyaan.this.startActivityForResult(intent, 201);
        }
        else if (requestCode==201) {
            try {
                byte[] encodeByte = Base64.decode(data.getStringExtra("image"), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);

                if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {
                    dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                    pertanyaanID[i] = PertanyaanID;
                    jawaban[i]      = data.getStringExtra("image");

                    i++;
                    complete++;
                }
                else {
                    pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = PertanyaanID;
                    jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = data.getStringExtra("image");
                }

                Bitmap thumbnail = bitmap;
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File getImage = (ActivityPertanyaan.this).getExternalCacheDir();
                File destination = new File(getImage.getPath(), PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID) + ".jpeg");

                realm.beginTransaction();
                ModelPertanyaanRealm model = new ModelPertanyaanRealm();
                model.setRealID(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                model.setId(PertanyaanID);
                model.setFormID(intent.getStringExtra(appConfig.FORM_ID));
                model.setDestination(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID) + ".jpeg");
                model.setJawaban(data.getStringExtra("image"));
                realm.copyToRealmOrUpdate(model);
                realm.commitTransaction();

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                tvComplete.setText("Complete " + String.valueOf(complete) + "/" + String.valueOf(jumlah));

                dataListSesungguhnya.clear();
                dataList.clear();
                adapter.notifyDataSetChanged();
                LoadData();

            }
            catch (Exception e) {
            }
        }
        else if (requestCode == 1) {

            try {
                String message = data.getStringExtra("MESSAGE");

                if (message.equals("ARRAY")) {

                    if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {
                        dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                        pertanyaanID[i] = PertanyaanID;
                        jawaban[i]      = message;
                        i++;
                        complete++;
                    }
                    else {
                        pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                                = PertanyaanID;
                        jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                                = message;
                    }

                    tvComplete.setText("Complete " + String.valueOf(complete) + "/" + String.valueOf(jumlah));

                    realm.beginTransaction();
                    ModelPertanyaanRealm model = new ModelPertanyaanRealm();
                    model.setRealID(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                    model.setId(PertanyaanID);
                    model.setFormID(intent.getStringExtra(appConfig.FORM_ID));
                    model.setJawaban("Sudah Dijawab, Klik Untuk Melihat Detail");
                    realm.copyToRealmOrUpdate(model);
                    realm.commitTransaction();


                    dataListSesungguhnya.clear();
                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    LoadData();

                }
                else if (message.equals("EMPTY")) {

                }
                else {
                    if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {
                        dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                        pertanyaanID[i] = PertanyaanID;
                        jawaban[i]      = message;
                        i++;
                        complete++;
                    }
                    else {

                        pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                                = PertanyaanID;
                        jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                                = message;
                    }

                    //dataList.get(Integer.parseInt(POSITION)).setJawaban(message);
                    // adapter.notifyDataSetChanged();
                    tvComplete.setText("Complete " + String.valueOf(complete) + "/" + String.valueOf(jumlah));

                    realm.beginTransaction();
                    ModelPertanyaanRealm model = new ModelPertanyaanRealm();
                    model.setRealID(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                    model.setId(PertanyaanID);
                    model.setFormID(intent.getStringExtra(appConfig.FORM_ID));
                    model.setJawaban(message);
                    realm.copyToRealmOrUpdate(model);
                    realm.commitTransaction();


                    dataListSesungguhnya.clear();
                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    LoadData();

                }
           }
           catch (Exception e) {

           }
        }
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }


    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(ActivityPertanyaan.this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityPertanyaan.this, permission)) {
                ActivityCompat.requestPermissions(ActivityPertanyaan.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(ActivityPertanyaan.this, new String[]{permission}, requestCode);
            }
        }
        else {
            if (requestCode == 1) {
                askForGPS();
            }
            else if (requestCode == 2) {
                askForPermission(READ_EXTERNAL_STORAGE,READ_EXST);
            }
            else if (requestCode == 3) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                startActivityForResult(getPickImageChooserIntent(), 200);
            }
            else if (permission == READ_PHONE_STATE) {
                try {
                    TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                    String SerialNumber  = tm.getDeviceId();
                    Send(SerialNumber);
                }
                catch (Exception e) {
                    Send("0");
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION :
                askForGPS();
                break;
            case CAMERA_EXST:
                askForPermission(READ_EXTERNAL_STORAGE,READ_EXST);
                break;
            case READ_EXST:
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                startActivityForResult(getPickImageChooserIntent(), 200);
        }

        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case 1:
                    TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    String SerialNumber = tm.getSimSerialNumber();
                    Send(SerialNumber);
                    break;
            }
        }
    }

    public void GetImage () {
        askForPermission(CAMERA,CAMERA_EXST);
    }


    public Intent getPickImageChooserIntent() {
        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = (ActivityPertanyaan.this).getPackageManager();

        Intent captureIntent;

        if (mController.getUserProfile(appConfig.TYPE).equals("CAMERA")) {
            captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        else {
            captureIntent = new Intent(Intent.ACTION_PICK);
            captureIntent.setType("image/*");
        }



        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select Source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = ((Activity) ActivityPertanyaan.this).getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), PertanyaanID + "ossas.jpeg"));
        }
        return outputFileUri;
    }


    public void OnDate () {

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                        if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {
                           // dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                            pertanyaanID[i] = PertanyaanID;
                            jawaban[i]      = String.format(
                                    "%02d-%02d-%d", dayOfMonth, monthOfYear+1, year
                            );
                            i++;
                            complete++;
                        }
                        else {

                            pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                                    = PertanyaanID;
                            jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                                    = String.format(
                                    "%02d-%02d-%d", dayOfMonth, monthOfYear+1, year
                            );
                        }

                        realm.beginTransaction();
                        ModelPertanyaanRealm model = new ModelPertanyaanRealm();
                        model.setRealID(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                        model.setId(PertanyaanID);
                        model.setFormID(intent.getStringExtra(appConfig.FORM_ID));
                        model.setJawaban(String.format(
                                "%02d-%02d-%d", dayOfMonth, monthOfYear+1, year
                        ));

                        realm.copyToRealmOrUpdate(model);
                        realm.commitTransaction();


                        tvComplete.setText("Complete " + String.valueOf(complete) + "/" + String.valueOf(jumlah));

                        dataListSesungguhnya.clear();
                        dataList.clear();
                        adapter.notifyDataSetChanged();
                        LoadData();



                    }
                },
                year,
                month,
                day
        );
        //dpd.showYearPickerFirst(true);
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    public void OnTime () {
        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min  = c.get(Calendar.MINUTE);
        int sec  = c.get(Calendar.SECOND);
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog dpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {


                if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {
                   // dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                    pertanyaanID[i] = PertanyaanID;
                    jawaban[i]      = String.format(
                            "%02d:%02d:%02d", hourOfDay, minute, second
                    );
                    i++;
                    complete++;
                }
                else {

                    pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = PertanyaanID;
                    jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = String.format(
                            "%02d:%02d:%02d", hourOfDay, minute, second
                    );
                }

                realm.beginTransaction();
                ModelPertanyaanRealm model = new ModelPertanyaanRealm();
                model.setRealID(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                model.setId(PertanyaanID);
                model.setFormID(intent.getStringExtra(appConfig.FORM_ID));
                model.setJawaban(String.format(
                        "%02d:%02d:%02d", hourOfDay, minute, second
                ));

                realm.copyToRealmOrUpdate(model);
                realm.commitTransaction();



                dataList.get(Integer.parseInt(POSITION)).setJawaban(
                        String.format(
                                "%02d:%02d:%02d", hourOfDay, minute, second
                        ));
                adapter.notifyDataSetChanged();
                tvComplete.setText("Complete " + String.valueOf(complete) + "/" + String.valueOf(jumlah));

                dataListSesungguhnya.clear();
                dataList.clear();
                adapter.notifyDataSetChanged();
                LoadData();



            }
        },hour,min,true);
        dpd.setVersion(TimePickerDialog.Version.VERSION_2);
        dpd.show(getFragmentManager(), "Timepickerdialog");
    }

    public void PlaySnake (String msg) {
        Snackbar mysnack = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_SHORT);

        mysnack.getView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) { }
            @Override
            public void onViewDetachedFromWindow(View v) {
                if (findViewById(android.R.id.content) instanceof FloatingActionButton)
                    findViewById(android.R.id.content).setTranslationY(0);
            }
        });
        mysnack.show();
    }

}
