package com.production.ads.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.production.ads.App.appController;
import com.production.ads.R;
import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.production.ads.Activity.ActivityRadioButton.POSITION;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView zXingScannerView;
    private appController mController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        mController = appController.getInstance();
        scan();
    }

    public void scan() {
        zXingScannerView =new ZXingScannerView(getApplicationContext());
        setContentView(zXingScannerView);
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        zXingScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        zXingScannerView.resumeCameraPreview(this);
        Intent intennyaSaja        = getIntent();
        String pos                 = intennyaSaja.getStringExtra(POSITION);
        Intent intent=new Intent();
        intent.putExtra("MESSAGE", result.getText());
        intent.putExtra(POSITION, pos);
        intent.putExtra(ActivityRadioButton.PERTANYAAN_ID, intennyaSaja.getStringExtra(ActivityRadioButton.PERTANYAAN_ID));
        setResult(1,intent);
        finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent intent = new Intent();
        setResult(3,intent);
        finish();
    }

}