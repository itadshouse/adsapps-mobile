package com.production.ads.Activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import com.production.ads.Adapter.AdapterRadioButton;
import com.production.ads.App.appController;
import com.production.ads.Model.ModelRadioButton;
import com.production.ads.R;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ActivityRadioButton extends Activity  {


    private RecyclerView mRecyclerView;
    private List<ModelRadioButton>     dataList;
    private AdapterRadioButton adapter;

    protected Handler               handler;

    public appController mController;

    private TextView info;
    private TextView tvInfoPertanyaan;
    private TextView tvTutup;

    public Intent intent;
    public static String ANSWER_GROUP = "answer_group";
    public static String POSITION = "position";
    public static String PERTANYAAN_ID = "pertanyaan_id";
    public static String JAWABAN = "jawaban";

    public static String FORM_ID = "formID";


    public String pos;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityRadioButton.this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_radiobutton);
        mController         = appController.getInstance();
        mRecyclerView       = findViewById(R.id.search_result);
        info                = findViewById(R.id.info);
        tvInfoPertanyaan    = findViewById(R.id.tvInfoPertanyaan);
        tvTutup             = findViewById(R.id.tvTutup);

        tvTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        handler = new Handler();
        dataList = new ArrayList<>();
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ActivityRadioButton.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        adapter = new AdapterRadioButton(ActivityRadioButton.this, dataList, mRecyclerView, mRecyclerView, false);
        mRecyclerView.setAdapter(adapter);

        intent = getIntent();
        pos    = intent.getStringExtra(POSITION);

        try {
           // JSONObject jObj = new JSONObject(intent.getStringExtra(ANSWER_GROUP));
            JSONArray jData = new JSONArray(intent.getStringExtra(ANSWER_GROUP));

            tvInfoPertanyaan.setText("");


            for (int i = 0; i < jData.length(); i++) {
                final ModelRadioButton product = new ModelRadioButton();
                String  row = jData.get(i).toString();
                product.setId(String.valueOf(i+1));
                product.setName(row);
                dataList.add(product);
            }

            info.setVisibility(View.GONE);
            adapter.notifyDataSetChanged();
            adapter.setLoaded();

        } catch (JSONException e) {

        }



    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }



}