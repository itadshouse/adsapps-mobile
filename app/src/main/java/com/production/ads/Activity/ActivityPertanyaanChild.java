package com.production.ads.Activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.production.ads.Adapter.AdapterPertanyaanChild;
import com.production.ads.App.appConfig;
import com.production.ads.App.appController;
import com.production.ads.Model.ModelPertanyaan;
import com.production.ads.Model.ModelPertanyaanRealm;
import com.production.ads.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class ActivityPertanyaanChild extends AppCompatActivity {


    private RecyclerView mRecyclerView;
    public List<ModelPertanyaan> dataList;
    private AdapterPertanyaanChild adapter;
    private appController mController;

    public Intent intent;

    public String[] pertanyaanID;
    public String[] jawaban;
    public String[] type;

    Button btnSubmit;

    private static final int GPS_SETTINGS = 0x7;

    LocationRequest mLocationRequest;
    PendingResult<LocationSettingsResult> result;
    GoogleApiClient client;


    private GPSTracker gps;
    private Double latDouble = 0.0;
    private Double lotDouble = 0.0;

    public String PertanyaanID = "";
    public String POSITION;
    public String MAX = "";
    public String JENIS = "";
    public String TIPE_TULISAN = "";


    private static final int LOCATION = 1;
    private static final int READ_EXST = 2;
    private static final int CAMERA_EXST = 3;

    public List<ModelPertanyaan> dataListSesungguhnya;

    public RelativeLayout LayoutSubmit;
    public LinearLayout LayoutJawaban;
    public RelativeLayout LayoutTransparant;

    public EditText tvEmail;
    public EditText tvJawaban;
    public EditText tvNumber;
    public EditText tvNote;
    public EditText tvLower;
    public EditText tvFloat;
    public TextView tvTutup;
    public Button buttonSelesai;

    public int i = 0;
    private Realm realm;
    private Toolbar toolbar;
    public Intent intennyaSaja ;
    String pos;

    private ArrayList <String> checkedValue = new ArrayList<String>();
    private ArrayList <String> checkedValueID = new ArrayList<String>();
    private ArrayList <String> checkedPertanyaan = new ArrayList<String>();
    private ArrayList <String> checkedType = new ArrayList<String>();

    public void AddPertanyaan(String data) {
        checkedPertanyaan.add(data);
    }

    public void Add (String data) {
        checkedValue.add(data);
    }

    public void AddID (String data) {
        checkedValueID.add(data);
    }

    public void AddType (String data) {
        checkedType.add(data);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pertanyaan_child_2);
        mController = appController.getInstance();
        mRecyclerView   =  findViewById(R.id.search_result);



        pertanyaanID    = new String[500];
        jawaban         = new String[500];
        type            = new String[500];


        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Toolbar tb      = findViewById(R.id.toolBar);
        TextView title  = tb.findViewById(R.id.toolbarTitle);
        ImageView back  = tb.findViewById(R.id.back);
        ImageView ref   = tb.findViewById(R.id.ref);
        TextView offline    = tb.findViewById(R.id.offline);
        offline.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ref.setVisibility(View.GONE);



        dataList                = new ArrayList<>();
        dataListSesungguhnya    = new ArrayList<>();

        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ActivityPertanyaanChild.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        adapter = new AdapterPertanyaanChild(ActivityPertanyaanChild.this,
                dataListSesungguhnya, mRecyclerView, mRecyclerView);
        mRecyclerView.setAdapter(adapter);


        intennyaSaja        = getIntent();
        intent              = getIntent();
        pos                 = intennyaSaja.getStringExtra(POSITION);

        title.setText(intennyaSaja.getStringExtra("NAME"));

        LayoutJawaban       = findViewById(R.id.LayoutJawaban);
        LayoutSubmit        = findViewById(R.id.LayoutSubmit);
        LayoutTransparant   = findViewById(R.id.LayoutTransparant);

        tvNumber            = findViewById(R.id.tvNumber);
        tvEmail             = findViewById(R.id.tvEmail);
        tvJawaban           = findViewById(R.id.tvJawaban);
        tvNote              = findViewById(R.id.tvNote);
        tvLower             = findViewById(R.id.tvLower);
        tvFloat             = findViewById(R.id.tvFloat);
        tvTutup             = findViewById(R.id.tvTutup);
        buttonSelesai       = findViewById(R.id.buttonSelesai);
        btnSubmit           = findViewById(R.id.btnSubmit);

        LayoutJawaban.setVisibility(View.GONE);
        LayoutSubmit.setVisibility(View.VISIBLE);



        buttonSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvJawaban.getText().toString().equals("")) {
                    Menjawab(tvJawaban);
                }
                else if (!tvEmail.getText().toString().equals("")) {
                    if (isValidEmail(tvEmail.getText().toString())) {
                        Menjawab(tvEmail);
                    }
                    else {
                        PlaySnake("Email Tidak Valid");
                    }
                }
                else if (!tvNumber.getText().toString().equals("")) {
                    Menjawab(tvNumber);
                }
                else if (!tvNote.getText().toString().equals("")) {
                    Menjawab(tvNote);
                }
                else if (!tvFloat.getText().toString().equals("")) {
                    Menjawab(tvFloat);
                }
                else if (!tvLower.getText().toString().equals("")) {
                    Menjawab(tvLower);
                }
                else {
                    PlaySnake("Isi Jawaban Terlebih Dahulu");
                }
            }
        });

        tvTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
                LayoutTransparant.setVisibility(View.GONE);
                LayoutJawaban.setVisibility(View.GONE);
                LayoutSubmit.setVisibility(View.VISIBLE);
            }
        });


        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(ActivityPertanyaanChild.this).build();

        try {
            realm = Realm.getInstance(realmConfiguration);
        } catch (RealmMigrationNeededException e){
            try {
                Realm.deleteRealm(realmConfiguration);
                realm = Realm.getInstance(realmConfiguration);
            } catch (Exception ex){
                throw ex;
            }
        }


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean submit = true;
                String numberNya = "";
                boolean sudahDidefinisikan = false;

                for (int i = 0; i < dataList.size(); i++) {
                    if (dataList.get(i).getMandatory().equals("*")
                            && !dataList.get(i).getQuestion_type().equals("Section")
                            ) {
                        if (dataList.get(i).getJawaban().equals("")) {

                            if (!sudahDidefinisikan) {
                                numberNya = dataList.get(i).getPosition();
                                sudahDidefinisikan = true;
                            }
                            submit = false;
                        }
                    }
                }

                if (submit) {
                    btnSubmit.setEnabled(false);
                    for (int j = 0; j < i; j++) {


                        String idChild;

                        try {
                            if (type[j].toString().equals("IMAGE")) {
                                try {
                                    idChild = intent.getStringExtra("ID_PERTANYAAN") + "FORM" + intent.getStringExtra("ID_FORM") + "CHILD" + pertanyaanID[j];
                                    byte[] encodeByte = Base64.decode(jawaban[j].toString(), Base64.DEFAULT);
                                    Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);

                                    Bitmap thumbnail = bitmap;
                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                                    File getImage = (ActivityPertanyaanChild.this).getExternalCacheDir();
                                    File destination = new File(getImage.getPath(), idChild + ".jpeg");

                                    FileOutputStream fo;
                                    try {
                                        destination.createNewFile();
                                        fo = new FileOutputStream(destination);
                                        fo.write(bytes.toByteArray());
                                        fo.close();
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    idChild = idChild + ".jpeg";
                                }
                                catch (Exception e) {
                                    idChild = "";
                                }
                            }
                            else {
                                idChild = "";
                            }
                        }
                        catch (Exception e) {
                            idChild = "";
                        }


                        realm.beginTransaction();
                        ModelPertanyaanRealm model = new ModelPertanyaanRealm();
                        model.setRealID(intent.getStringExtra("ID_PERTANYAAN") + "FORM" + intent.getStringExtra("ID_FORM") + "CHILD" + pertanyaanID[j]);
                        model.setIdChild("CHILD");
                        model.setId(pertanyaanID[j]);
                        model.setFormID(intent.getStringExtra("ID_FORM"));
                        model.setJawaban(jawaban[j]);
                        model.setDestination(idChild);
                        realm.copyToRealmOrUpdate(model);
                        realm.commitTransaction();

                        Add(jawaban[j]);
                        AddID(pertanyaanID[j]);
                    }

                    if (i > 0) {
                        Intent intent=new Intent();
                        intent.putExtra("MESSAGE", "ARRAY");

                        intent.putStringArrayListExtra("ID", checkedValueID);
                        intent.putStringArrayListExtra("JAWABAN", checkedValue);
                        intent.putStringArrayListExtra("PERTANYAAN", checkedPertanyaan);
                        intent.putStringArrayListExtra("TYPE", checkedType);

                        intent.putExtra(POSITION, pos);
                        intent.putExtra(ActivityRadioButton.PERTANYAAN_ID, intennyaSaja.getStringExtra(ActivityRadioButton.PERTANYAAN_ID));
                        setResult(1,intent);
                        finish();
                    }
                    else {
                        finish();
                    }
                }
                else {
                    PlaySnake("Isi Jawaban Dari Pertanyaan No. " + numberNya);
                    btnSubmit.setEnabled(true);
                }

            }
        });

        client = new GoogleApiClient.Builder(this)
                .addApi(AppIndex.API)
                .addApi(LocationServices.API)
                .build();

        LoadData();


    }


    private void LoadData() {
        try {
            JSONArray jData = new JSONArray(intennyaSaja.getStringExtra("DATA"));
            for (int k = 0; k < jData.length(); k++) {
                final ModelPertanyaan product = new ModelPertanyaan();
                JSONObject row = jData.getJSONObject(k);

                JSONObject  tipe = row.getJSONObject("tipe");
                product.setDestination("null");

                RealmResults<ModelPertanyaanRealm> rs = realm.where(ModelPertanyaanRealm.class).
                        equalTo("realID", intennyaSaja.getStringExtra("ID_PERTANYAAN") + "FORM" + intennyaSaja.getStringExtra("ID_FORM") + "CHILD" + row.getString("namainputan")).findAll();
                product.setJawaban("");
                if (rs.size() > 0) {
                    if (tipe.getString("name").toString().equals("Image") || tipe.getString("name").toString().equals("Gallery")) {
                        try {
                            File getImage = (ActivityPertanyaanChild.this).getExternalCacheDir();
                            File destination = new File(getImage.getPath(), rs.get(0).getDestination());
                            product.setDestination(destination.toString());

                            product.setJawaban(rs.get(0).getJawaban());
                            pertanyaanID[i] = rs.get(0).getId();
                            jawaban[i]      = rs.get(0).getJawaban();
                            i++;

                        }
                        catch (Exception e) {
                            realm.beginTransaction();
                            rs.clear();
                            realm.commitTransaction();
                        }
                    }
                    else {
                        product.setJawaban(rs.get(0).getJawaban());
                        pertanyaanID[i] = rs.get(0).getId();
                        jawaban[i]      = rs.get(0).getJawaban();
                        i++;
                    }
                }
                else {
                    product.setJawaban("");
                    for (int b = 0; b < i; b++) {
                        if (pertanyaanID[b].equals(row.getString("namainputan"))) {
                            if (type[b].equals("IMAGE")) {
                                try {
                                    //byte[] encodeByte = Base64.decode(jawaban[b], Base64.DEFAULT);
                                    // Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                                    product.setJawaban(jawaban[b]);

                                    // product.setImage(bitmap);
                                    File getImage = (ActivityPertanyaanChild.this).getExternalCacheDir();
                                    File destination = new File(getImage.getPath(), pertanyaanID[b]+ "FORM" + intent.getStringExtra(appConfig.FORM_ID) + ".jpeg");
                                    product.setDestination(destination.toString());
                                }
                                catch (Exception e) {
                                }
                            }
                            else {
                                product.setJawaban(jawaban[b]);
                            }
                        }
                    }
                }


                product.setAnswer_group(row.getString("option"));
                product.setId(row.getString("namainputan"));
                product.setPertanyaan(row.getString("label"));
                product.setQuestion_type(tipe.getString("name"));
                product.setNomor(String.valueOf(i+1));
                product.setMaximum(row.getString("maximum"));
                product.setTipeTulisan(row.getString("tipetulisan"));
                product.setUrutan("");
                product.setImage(null);


                product.setChild("");
                product.setMandatory("");

                product.setChild("");

                if (row.getString("mandatory").equals("Y")) {
                    product.setMandatory("*");
                }
                else {
                    product.setMandatory("");
                }

                product.setPosition(row.getString("position"));
                product.setKondisi(row.getString("kondisi"));


                if (row.getString("desc").equals("null")) {
                    product.setDeskripsi("");
                }
                else {
                    product.setDeskripsi(row.getString("desc"));
                }

                if (!row.getString("kondisi").equals("null")) {
                    JSONArray detailKondisi = new JSONArray(row.getString("detail_kondisi"));
                    for (int kk = 0; kk < detailKondisi.length(); kk++) {
                        JSONObject  r = detailKondisi.getJSONObject(kk);
                        if (kk == 0) {
                            product.setDetail_kondisi_id(r.getString("pertanyaanID"));
                            product.setDetail_kondisi_notasi(r.getString("notasi"));
                            product.setDetail_kondisi_nilai(r.getString("nilai"));
                        }
                        else {
                            product.setDetail_kondisi_id_2(r.getString("pertanyaanID"));
                            product.setDetail_kondisi_notas_2(r.getString("notasi"));
                            product.setDetail_kondisi_nilai_2(r.getString("nilai"));
                        }
                    }
                }
                else {
                    product.setDetail_kondisi_id("");
                    product.setDetail_kondisi_notasi("");
                    product.setDetail_kondisi_id_2("");
                    product.setDetail_kondisi_notas_2("");
                    product.setDetail_kondisi_nilai("");
                    product.setDetail_kondisi_nilai_2("");
                }

                if (row.getString("ismultiple").equals("Y")) {
                    product.setMultiple(true);
                }
                else {
                    product.setMultiple(false);
                }

                dataList.add(product);
            }

            for (int n = 0; n < dataList.size(); n++) {
                ModelPertanyaan product = dataList.get(n);

                boolean hilang;

                if (product.getKondisi().toString().equals("null") ) {
                    hilang = false;
                }
                else {
                    hilang = true;
                    String id_kondisi = product.getDetail_kondisi_id();
                    if (product.getDetail_kondisi_id() != null && product.getDetail_kondisi_id_2() != null) {
                        String jawaban = "";


                        for (int senin = 0; senin < dataList.size(); senin++) {

                            if (product.getDetail_kondisi_id().equals(dataList.get(senin).getId())) {

                                if (dataList.get(senin).getJawaban().equals("")) {
                                    hilang = true;
                                }
                                else {
                                    jawaban = dataList.get(senin).getJawaban();
                                }
                            }
                        }

                        String jawaban2 = "";

                        for (int senin = 0; senin < dataList.size(); senin++) {
                            if (product.getDetail_kondisi_id_2().equals(dataList.get(senin).getId())) {

                                if (dataList.get(senin).getJawaban().equals("")) {
                                    hilang = true;
                                }
                                else {
                                    jawaban2 = dataList.get(senin).getJawaban();
                                }
                            }
                        }
                        if (product.getKondisi().equals("AND") && !jawaban.equals("") && !jawaban2.equals("")) {
                            boolean one = MapString.opByName.get(product.getDetail_kondisi_notasi()).calculate(jawaban,
                                    product.getDetail_kondisi_nilai());

                            boolean two = MapString.opByName.get(product.getDetail_kondisi_notas_2()).calculate(jawaban2,
                                    product.getDetail_kondisi_nilai_2());

                            if (one && two) {
                                hilang = false;
                            }
                        }
                        else if (product.getKondisi().equals("OR") && !jawaban.equals("") && !jawaban2.equals("")) {
                            boolean one = MapString.opByName.get(product.getDetail_kondisi_notasi()).calculate(jawaban,
                                    product.getDetail_kondisi_nilai());

                            boolean two = MapString.opByName.get(product.getDetail_kondisi_notas_2()).calculate(jawaban2,
                                    product.getDetail_kondisi_nilai_2());

                            if (one || two) {
                                hilang = false;
                            }
                        }
                        else {
                            hilang = true;
                        }
                    }
                    else if (product.getDetail_kondisi_id() != null) {
                        String jawaban = "";

                        for (int senin = 0; senin < dataList.size(); senin++) {
                            if (id_kondisi.equals(dataList.get(senin).getId())) {

                                if (dataList.get(senin).getJawaban().equals("")) {
                                    hilang = true;
                                }
                                else {
                                    jawaban = dataList.get(senin).getJawaban();
                                }
                            }
                        }

                        if (!jawaban.equals("")) {
                            boolean one = MapString.opByName.get(product.getDetail_kondisi_notasi()).calculate(jawaban,
                                    product.getDetail_kondisi_nilai());

                            if (one) {
                                hilang = false;
                            }
                        }
                        else {
                            hilang = true;

                        }
                    }
                }
                if (hilang) {
                    Log.d("TESTING_KOK", "TES");

                    for (int b = 0; b < i; b++) {
                        if (pertanyaanID[b].equals(product.getId())) {
                            jawaban[b] = "";
                        }
                    }


                }
                else {
                    dataListSesungguhnya.add(product);
                }
            }


            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
        }
    }

    public void Menjawab (EditText editText) {

        boolean lanjut = true;

        if (MAX.equals("") || MAX.equals(null) || MAX.equals("0")) {
            lanjut = true;
        }
        else {
            if (JENIS.equals("")) {
                lanjut = true;
            }
            else {

                try {
                    if (Float.parseFloat(editText.getText().toString()) <= Integer.parseInt(MAX)) {
                        lanjut = true;
                    }
                    else {
                        lanjut = false;
                        Toast.makeText(ActivityPertanyaanChild.this, "Jawaban Terlalu Panjang", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e) {
                    try {
                        if (editText.length() <= Integer.parseInt(MAX)) {
                            lanjut = true;
                        }
                        else {
                            lanjut = false;
                            Toast.makeText(getApplicationContext(), "Jawaban Terlalu Panjang", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception B) {
                        lanjut = true;
                    }
                }
            }
        }



        if (lanjut) {

            if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {

                Log.d("TESTINGGG","OKK");

                dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                pertanyaanID[i] = PertanyaanID;
                jawaban[i]      = editText.getText().toString();
                type[i]         = "NORMAL";
                i++;
            }
            else {
                pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())] = PertanyaanID;
                jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())] = editText.getText().toString();
                type[i]         = "NORMAL";
            }
            Up(editText.getText().toString());
        }
    }

    public void Up (String jawaban) {
        adapter.nomor = 1;

        //dataList.get(Integer.parseInt(POSITION)).setJawaban(jawaban.replaceAll("\\[", "").replaceAll("\\]",""));
        //adapter.notifyDataSetChanged();

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
        LayoutTransparant.setVisibility(View.GONE);
        LayoutJawaban.setVisibility(View.GONE);
        LayoutSubmit.setVisibility(View.VISIBLE);

        dataListSesungguhnya.clear();
        dataList.clear();
        adapter.notifyDataSetChanged();
        LoadData();

    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(ActivityPertanyaanChild.this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityPertanyaanChild.this, permission)) {
                ActivityCompat.requestPermissions(ActivityPertanyaanChild.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(ActivityPertanyaanChild.this, new String[]{permission}, requestCode);
            }
        }
        else {
            if (requestCode == 1) {
                askForGPS();
            }
            else if (requestCode == 2) {
                askForPermission(READ_EXTERNAL_STORAGE,READ_EXST);
            }
            else if (requestCode == 3) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                startActivityForResult(getPickImageChooserIntent(), 200);
            }

        }
    }



    private void askForGPS(){
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(client, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(ActivityPertanyaanChild.this, GPS_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });

        try {
            gps = new GPSTracker(this);
            if (gps.canGetLocation) {
                latDouble = gps.getLat();
                lotDouble = gps.getLot();
            }
            else {
                showSettingAlert();
            }
        } catch (Exception e) {
            latDouble = 0.0;
            lotDouble = 0.0;
        }
    }

    public void showSettingAlert () {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(ActivityPertanyaanChild.this);

        alertDialog.setTitle("GPS Belum Aktif");
        alertDialog.setMessage("Untuk pendataan, kami memerlukan data lokasi anda");

        alertDialog.setPositiveButton("Konfigurasi GPS", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent (Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);

            }
        });

        alertDialog.setNegativeButton("Lanjtukan Tanpa Data Lokasi", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                latDouble = 0.0;
                lotDouble = 0.0;
            }
        });

        alertDialog.show();

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==200 && resultCode  ==Activity.RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);
            Intent intent = new Intent(ActivityPertanyaanChild.this, ActivityPrescriptionInput.class);
            intent.putExtra("id", "");
            intent.putExtra("imageUri", imageUri.toString());
            intent.putExtra("cek", "false");
            ActivityPertanyaanChild.this.startActivityForResult(intent, 201);
        }
        else if (requestCode==201) {
            try {
                byte[] encodeByte = Base64.decode(data.getStringExtra("image"), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);

                if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {
                    dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                    pertanyaanID[i] = PertanyaanID;
                    jawaban[i]      = data.getStringExtra("image");
                    type[i]         = "IMAGE";

                    i++;
                }
                else {
                    pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = PertanyaanID;
                    jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = data.getStringExtra("image");
                    type[i]         = "IMAGE";
                }

                Bitmap thumbnail = bitmap;
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File getImage = ((Activity) ActivityPertanyaanChild.this).getExternalCacheDir();
                File destination = new File(getImage.getPath(), PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID) + ".jpeg");

                realm.beginTransaction();
                ModelPertanyaanRealm model = new ModelPertanyaanRealm();
                model.setRealID(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                model.setId(PertanyaanID);
                model.setFormID(intent.getStringExtra(appConfig.FORM_ID));
                model.setDestination(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID) + ".jpeg");
                model.setJawaban(data.getStringExtra("image"));
                realm.copyToRealmOrUpdate(model);
                realm.commitTransaction();

                FileOutputStream fo;
                try {
                    Log.d("SAVE IMGA", "1");
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    Log.d("SAVE IMGA", "2");
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.d("SAVE IMGA", "3");
                    e.printStackTrace();
                }

                adapter.nomor = 1;
                //dataList.get(Integer.valueOf(POSITION)).setImage(bitmap);
                // dataList.get(Integer.valueOf(POSITION)).setJawaban(bitmapToBase64String(bitmap));
                //adapter.notifyDataSetChanged();

                dataListSesungguhnya.clear();
                dataList.clear();
                adapter.notifyDataSetChanged();
                LoadData();

            }
            catch (Exception e) {
            }
        }
        else if (requestCode == 1) {

            try {
                String message = data.getStringExtra("MESSAGE");


                if (message.equals("ARRAY")) {

                }
                else {
                    if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {
                        dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                        pertanyaanID[i] = PertanyaanID;
                        jawaban[i]      = message;
                        type[i]         = "NORMAL";
                        i++;
                    }
                    else {

                        pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                                = PertanyaanID;
                        jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                                = message;
                        type[i]         = "NORMAL";
                    }



                    adapter.nomor = 1;
                    // dataList.get(Integer.parseInt(POSITION)).setJawaban(message);
                    //adapter.notifyDataSetChanged();

                    realm.beginTransaction();
                    ModelPertanyaanRealm model = new ModelPertanyaanRealm();
                    model.setRealID(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                    model.setId(PertanyaanID);
                    model.setFormID(intent.getStringExtra(appConfig.FORM_ID));
                    model.setJawaban(message);
                    realm.copyToRealmOrUpdate(model);
                    realm.commitTransaction();

                    dataListSesungguhnya.clear();
                    dataList.clear();
                    adapter.notifyDataSetChanged();
                    LoadData();
                }
            }
            catch (Exception e) {

            }


        }
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION :
                askForGPS();
                break;
            case CAMERA_EXST:
                askForPermission(READ_EXTERNAL_STORAGE,READ_EXST);
                break;
            case READ_EXST:
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                startActivityForResult(getPickImageChooserIntent(), 200);
                break;
        }
    }

    public void GetImage () {
        askForPermission(CAMERA,CAMERA_EXST);
    }


    public Intent getPickImageChooserIntent() {

        Intent captureIntent;

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = ((Activity) ActivityPertanyaanChild.this).getPackageManager();

        if (mController.getUserProfile(appConfig.TYPE).equals("CAMERA")) {
            captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        else {
            captureIntent = new Intent(Intent.ACTION_PICK);
            captureIntent.setType("image/*");
        }
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select Source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));



        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = ((Activity) ActivityPertanyaanChild.this).getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), PertanyaanID + "ossas.jpeg"));
        }
        return outputFileUri;
    }


    public void OnDate () {

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                        if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {
                            dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                            pertanyaanID[i] = PertanyaanID;
                            jawaban[i]      = String.format(
                                    "%02d-%02d-%d", dayOfMonth, monthOfYear+1, year
                            );
                            type[i]         = "NORMAL";
                            i++;
                        }
                        else {

                            pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                                    = PertanyaanID;
                            jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                                    = String.format(
                                    "%02d-%02d-%d", dayOfMonth, monthOfYear+1, year
                            );
                            type[i]         = "NORMAL";
                        }

                        realm.beginTransaction();
                        ModelPertanyaanRealm model = new ModelPertanyaanRealm();
                        model.setRealID(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                        model.setIdChild(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                        model.setId(PertanyaanID);
                        model.setFormID(intent.getStringExtra(appConfig.FORM_ID));
                        model.setJawaban(String.format(
                                "%02d-%02d-%d", dayOfMonth, monthOfYear+1, year
                        ));

                        realm.copyToRealmOrUpdate(model);
                        realm.commitTransaction();

                        adapter.nomor = 1;
                        // dataList.get(Integer.parseInt(POSITION)).setJawaban(
                        // String.format(
                        //   "%02d-%02d-%d", dayOfMonth, monthOfYear+1, year
                        // ));
                        // adapter.notifyDataSetChanged();

                        dataListSesungguhnya.clear();
                        dataList.clear();
                        adapter.notifyDataSetChanged();
                        LoadData();

                    }
                },
                year,
                month,
                day
        );
        //dpd.showYearPickerFirst(true);
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    public void OnTime () {
        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min  = c.get(Calendar.MINUTE);
        int sec  = c.get(Calendar.SECOND);
        TimePickerDialog dpd = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {


                if (dataList.get(Integer.parseInt(POSITION)).getUrutan().equals("")) {
                    dataList.get(Integer.parseInt(POSITION)).setUrutan(String.valueOf(i));
                    pertanyaanID[i] = PertanyaanID;
                    jawaban[i]      = String.format(
                            "%02d:%02d:%02d", hourOfDay, minute, second
                    );
                    type[i]         = "NORMAL";
                    i++;
                }
                else {

                    pertanyaanID[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = PertanyaanID;
                    jawaban[Integer.parseInt(dataList.get(Integer.parseInt(POSITION)).getUrutan())]
                            = String.format(
                            "%02d:%02d:%02d", hourOfDay, minute, second
                    );
                    type[i]         = "NORMAL";
                }

                realm.beginTransaction();
                ModelPertanyaanRealm model = new ModelPertanyaanRealm();
                model.setRealID(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                model.setIdChild(PertanyaanID + "FORM" + intent.getStringExtra(appConfig.FORM_ID));
                model.setId(PertanyaanID);
                model.setFormID(intent.getStringExtra(appConfig.FORM_ID));
                model.setJawaban(String.format(
                        "%02d:%02d:%02d", hourOfDay, minute, second
                ));

                realm.copyToRealmOrUpdate(model);
                realm.commitTransaction();


                adapter.nomor = 1;
                /*
                dataList.get(Integer.parseInt(POSITION)).setJawaban(
                        String.format(
                                "%02d:%02d:%02d", hourOfDay, minute, second
                        ));
                adapter.notifyDataSetChanged();
                */

                dataListSesungguhnya.clear();
                dataList.clear();
                adapter.notifyDataSetChanged();
                LoadData();


            }
        },hour,min,true);
        dpd.setVersion(TimePickerDialog.Version.VERSION_2);
        dpd.show(getFragmentManager(), "Timepickerdialog");
    }

    public void PlaySnake (String msg) {
        Snackbar mysnack = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_SHORT);

        mysnack.getView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) { }
            @Override
            public void onViewDetachedFromWindow(View v) {
                if (findViewById(android.R.id.content) instanceof FloatingActionButton)
                    findViewById(android.R.id.content).setTranslationY(0);
            }
        });
        mysnack.show();
    }

}
