package com.production.ads.Offline;

/**
 * Created by Eko on 3/24/2018.
 */

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class ModelOffline extends RealmObject {

    @PrimaryKey
    private String CreatedAt;

    private String UserID;
    private String FormID;
    private String FormName;
    private String TimeStamp;
    private String Latitude;
    private String Longtitude;
    private String IMEI;
    private String IMEI_MD5;


    public String getFormName() {
        return FormName;
    }

    public void setFormName(String formName) {
        FormName = formName;
    }

    public String getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        CreatedAt = createdAt;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getFormID() {
        return FormID;
    }

    public void setFormID(String formID) {
        FormID = formID;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongtitude() {
        return Longtitude;
    }

    public void setLongtitude(String longtitude) {
        Longtitude = longtitude;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getIMEI_MD5() {
        return IMEI_MD5;
    }

    public void setIMEI_MD5(String IMEI_MD5) {
        this.IMEI_MD5 = IMEI_MD5;
    }
}
