package com.production.ads.Offline;

import io.realm.RealmObject;

/**
 * Created by Eko on 3/24/2018.
 */

import io.realm.annotations.RealmClass;

@RealmClass


public class ModelOfflineDetail extends RealmObject {

    private String CreatedAt;
    private String PertanyaanID;
    private String Jawaban;
    private String Index;

    public String getIndex() {
        return Index;
    }

    public void setIndex(String index) {
        Index = index;
    }

    public String getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        CreatedAt = createdAt;
    }

    public String getPertanyaanID() {
        return PertanyaanID;
    }

    public void setPertanyaanID(String pertanyaanID) {
        PertanyaanID = pertanyaanID;
    }

    public String getJawaban() {
        return Jawaban;
    }

    public void setJawaban(String jawaban) {
        Jawaban = jawaban;
    }
}
