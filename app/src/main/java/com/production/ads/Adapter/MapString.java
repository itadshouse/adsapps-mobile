package com.production.ads.Adapter;

import java.util.HashMap;
import java.util.Map;

public class MapString {
    interface Operation {
        boolean calculate(String a, String b);
    }

    static final Map<String,Operation> opByName = new HashMap<String,Operation>();
    static {
        opByName.put(">", new Operation() {
            public boolean calculate(String a, String b) {
                try {
                    if (Integer.parseInt(a) > Integer.parseInt(b)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                catch (Exception e) {
                    return false;
                }

            }
        });
        opByName.put("<", new Operation() {
            public boolean calculate(String a, String b) {
                try {
                    if (Integer.parseInt(a) < Integer.parseInt(b)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                catch (Exception e) {
                    return false;
                }

            }
        });
        opByName.put("<=", new Operation() {
            public boolean calculate(String a, String b) {
                try {
                    if (Integer.parseInt(a) <= Integer.parseInt(b)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                catch (Exception e) {
                    return false;
                }

            }
        });
        opByName.put(">=", new Operation() {
            public boolean calculate(String a, String b) {
                try {
                    if (Integer.parseInt(a) >= Integer.parseInt(b)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                catch (Exception e) {
                    return false;
                }
            }
        });

        opByName.put("==", new Operation() {
            public boolean calculate(String a, String b) {
                try {
                    if (a.equals(b)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                catch (Exception e) {
                    return false;
                }

            }
        });
        opByName.put("!=", new Operation() {
            public boolean calculate(String a, String b) {
                try {
                    if (!a.equals(b)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                catch (Exception e) {
                    return false;
                }

            }
        });

    }
}
