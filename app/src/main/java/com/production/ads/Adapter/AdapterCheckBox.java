package com.production.ads.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.production.ads.Activity.ActivityCheckBox;
import com.production.ads.Model.ModelCheckBox;
import com.production.ads.R;
import java.util.List;


public class AdapterCheckBox extends RecyclerView.Adapter {
	private final int VIEW_ITEM = 1;
	private final int VIEW_PROG = 0;

	private List<ModelCheckBox> productList;
	private int visibleThreshold = 10;
	private int lastVisibleItem, totalItemCount;
	private boolean loading = false;

    private static Context mContext;
	public static boolean fragment = false;
	boolean[] itemChecked;

	public AdapterCheckBox(Context context, List<ModelCheckBox> products, RecyclerView recyclerView, View v, boolean fragment) {
		productList = products;
        mContext = context;
		this.fragment = fragment;

		itemChecked = new boolean[99];


		if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

			final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
					.getLayoutManager();

			recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
						@Override
						public void onScrolled(RecyclerView recyclerView,
											   int dx, int dy) {
							super.onScrolled(recyclerView, dx, dy);
							totalItemCount = linearLayoutManager.getItemCount();
							lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
							Log.d("onScrolled", "totalItemCount : "+Integer.toString(totalItemCount));
                            Log.d("onScrolled", "lastVisibleItem: "+Integer.toString(lastVisibleItem));
                            Log.d("onScrolled", "visibleThreshold:" + visibleThreshold);
                            Log.d("onScrolled", "loading : "+(loading ? "true" : "false"));
						}
					});
		}
	}



	@Override
	public int getItemViewType(int position) {
		return productList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
		RecyclerView.ViewHolder vh;

		View v = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.row_checkbox, parent, false);

		vh = new ModelPenyakitViewHolder(mContext, v);

		return vh;
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

		if (holder instanceof ModelPenyakitViewHolder) {

         	final ModelCheckBox singleModelProduct =  productList.get(position);
            final ModelPenyakitViewHolder productView = ((ModelPenyakitViewHolder)holder);

            productView.product = singleModelProduct;

			if (singleModelProduct.getId().equals("ERROR")) {

			}
			else {

				productView.tvName.setText(singleModelProduct.getName());

				if (itemChecked[position])
					productView.checkBox1.setChecked(true);
				else
					productView.checkBox1.setChecked(false);

				productView.checkBox1.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						int i = position;
						// TODO Auto-generated method stub
						if (productView.checkBox1.isChecked()) {
							((ActivityCheckBox)mContext).Add(singleModelProduct.getName());
							itemChecked[i] = true;
						}
						else {
							((ActivityCheckBox)mContext).Kurang(singleModelProduct.getName());
							itemChecked[i] = false;
						}
					}
				});
			}
		} else {
			((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
			Log.d("AdapterProduct", "holder was NOT instanceof ModelProductViewHolder.");
		}
	}


	public void setLoaded() {
		loading = false;
	}

	@Override
	public int getItemCount() {
		return productList.size();
	}




	public static class ModelPenyakitViewHolder extends RecyclerView.ViewHolder {
		public TextView tvName;
		public ModelCheckBox product;
        private View mView;
		private Context con;

		private CheckBox checkBox1;



		public ModelPenyakitViewHolder(Context context, View v) {
			super(v);
            mView = v;
			tvName  = (TextView) v.findViewById(R.id.tvName);
			checkBox1 = (CheckBox) v.findViewById(R.id.checkBox1);
			con = context;
		}


    }

	public boolean contactExists(Context context, String number) {
		Uri lookupUri = Uri.withAppendedPath(
				ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
				Uri.encode(number));
		String[] mPhoneNumberProjection = { ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME };
		Cursor cur = context.getContentResolver().query(lookupUri,mPhoneNumberProjection, null, null, null);
		try {
			if (cur.moveToFirst()) {
				return true;
			}
		} finally {
			if (cur != null)
				cur.close();
		}
		return false;
	}




	public static class ProgressViewHolder extends RecyclerView.ViewHolder {
		public ProgressBar progressBar;

		public ProgressViewHolder(View v) {
			super(v);
			//progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		}
	}

    public boolean isLoading() {
        return loading;
    }
}
