package com.production.ads.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.production.ads.Activity.ActivityPertanyaan;
import com.production.ads.App.appConfig;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.production.ads.Activity.ActivityCheckBox;
import com.production.ads.Activity.ActivityMap;
import com.production.ads.Activity.ActivityPertanyaanChild;
import com.production.ads.Activity.ActivityRadioButton;
import com.production.ads.Activity.ScanActivity;
import com.production.ads.App.appController;
import com.production.ads.Model.ModelPertanyaan;
import com.production.ads.R;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class AdapterPertanyaanChild extends RecyclerView.Adapter {

    private List<ModelPertanyaan> model;
    private static Context mContext;
    private static appController mController;
    public ModelProductViewHolder productView;
    public int nomor = 1;

    public AdapterPertanyaanChild(Context context, List<ModelPertanyaan> model, RecyclerView recyclerView, View v) {
        this.model = model;
        mContext = context;
        mController = appController.getInstance();

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_pertanyaan, parent, false);
        vh = new ModelProductViewHolder(mContext, v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ModelProductViewHolder) {

            final ModelPertanyaan singleModelProduct =  model.get(position);
            productView = ((ModelProductViewHolder) holder);
            productView.pertanyaan = singleModelProduct;
            boolean hilang = false;

            if (!hilang) {
                productView.Layout_Show();
                productView.ifShow.setVisibility(View.VISIBLE);
                productView.ifHidden.setVisibility(View.GONE);

                if (!singleModelProduct.getQuestion_type().equals("Section")) {
                    if (singleModelProduct.getDeskripsi().equals("")) {
                        productView.tvDeskripsi.setVisibility(View.GONE);
                    }
                    else {
                        productView.tvDeskripsi.setVisibility(View.VISIBLE);
                        productView.tvDeskripsi.setText(singleModelProduct.getDeskripsi());
                    }
                    productView.cardViewPertanyaan.setVisibility(View.VISIBLE);
                    productView.cardViewLabel.setVisibility(View.GONE);

                    productView.tvPertanyaan.setText(singleModelProduct.getPertanyaan());
                    productView.tvIndex.setText(singleModelProduct.getPosition());
                    nomor++;
                    productView.tvJawaban.setText(singleModelProduct.getJawaban());

                    if (singleModelProduct.getQuestion_type().equals("Time")
                            || singleModelProduct.getQuestion_type().equals("Date")
                            || singleModelProduct.getQuestion_type().equals("Image")
                            || singleModelProduct.getQuestion_type().equals("Phone")
                            || singleModelProduct.getQuestion_type().equals("Email")
                            || singleModelProduct.getQuestion_type().equals("Barcode")
                            || singleModelProduct.getQuestion_type().equals("Location")
                            || singleModelProduct.getQuestion_type().equals("Dropdown")
                            || singleModelProduct.getQuestion_type().equals("Group") ||

                            singleModelProduct.getMaximum().toString().equals(null) || singleModelProduct.getMaximum().toString().equals("") || singleModelProduct.getMaximum().toString().equals("0")) {
                        productView.tvQuestionType.setText(singleModelProduct.getQuestion_type());
                    }
                    else {
                        productView.tvQuestionType.setText(singleModelProduct.getQuestion_type() + " - Max : " + singleModelProduct.getMaximum().toString());
                    }

                    productView.tvMandatory.setText(singleModelProduct.getMandatory());


                    productView.imgImage.setImageBitmap(singleModelProduct.getImage());

                    if (!singleModelProduct.getChild().equals("")) {

                    }
                    else {
                        productView.tvJawaban.setVisibility(View.VISIBLE);
                        productView.mRecyclerView.setVisibility(View.GONE);
                    }

                    boolean janganLanjut = false;
                    if (singleModelProduct.getImage() == null) {
                        productView.imgImage.setVisibility(View.GONE);
                        productView.tvJawaban.setVisibility(View.VISIBLE);
                    }
                    else {
                        productView.imgImage.setVisibility(View.VISIBLE);
                        productView.tvJawaban.setVisibility(View.GONE);
                        janganLanjut = true;

                    }

                    if (janganLanjut) {

                    }
                    else {
                        if (!singleModelProduct.getDestination().equals("null")) {
                            Picasso.with(mContext).load("file://"+singleModelProduct.getDestination()).placeholder(mContext.getResources()
                                    .getDrawable(R.drawable.blank))
                                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                                    .into(productView.imgImage);

                            productView.imgImage.setVisibility(View.VISIBLE);
                            productView.tvJawaban.setVisibility(View.GONE);
                        }
                        else {
                            productView.imgImage.setVisibility(View.GONE);
                            productView.tvJawaban.setVisibility(View.VISIBLE);
                        }
                    }

                    productView.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((ActivityPertanyaanChild)mContext).tvJawaban.setVisibility(View.GONE);
                            ((ActivityPertanyaanChild)mContext).tvEmail.setVisibility(View.GONE);
                            ((ActivityPertanyaanChild)mContext).tvNumber.setVisibility(View.GONE);
                            ((ActivityPertanyaanChild)mContext).tvNote.setVisibility(View.GONE);
                            ((ActivityPertanyaanChild)mContext).tvLower.setVisibility(View.GONE);
                            ((ActivityPertanyaanChild)mContext).tvFloat.setVisibility(View.GONE);
                            ((ActivityPertanyaanChild)mContext).tvNote.setText("");
                            ((ActivityPertanyaanChild)mContext).tvNumber.setText("");
                            ((ActivityPertanyaanChild)mContext).tvJawaban.setText("");
                            ((ActivityPertanyaanChild)mContext).tvEmail.setText("");
                            ((ActivityPertanyaanChild)mContext).tvLower.setText("");
                            ((ActivityPertanyaanChild)mContext).tvFloat.setText("");
                            switch (singleModelProduct.getQuestion_type()) {
                                case "Dropdown" :
                                    Intent intent =new Intent(mContext,ActivityCheckBox.class);
                                    intent.putExtra(ActivityCheckBox.POSITION, String.valueOf(position));
                                    intent.putExtra(ActivityCheckBox.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                    ((ActivityPertanyaanChild)mContext).startActivityForResult(intent, 1);
                                    break;
                                case "Group" :
                                    break;
                                case "Radio" :
                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();

                                    Intent radioButton = new Intent(mContext,ActivityRadioButton.class);
                                    radioButton.putExtra(ActivityRadioButton.POSITION, String.valueOf(position));
                                    radioButton.putExtra(ActivityRadioButton.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                    ((ActivityPertanyaanChild)mContext).startActivityForResult(radioButton, 1);
                                    break;
                                case "Location" :
                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();

                                    Intent map = new Intent(mContext,ActivityMap.class);
                                    map.putExtra(ActivityRadioButton.POSITION, String.valueOf(position));
                                    map.putExtra(ActivityRadioButton.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                    ((ActivityPertanyaanChild)mContext).startActivityForResult(map, 1);
                                    break;
                                case "Barcode" :
                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();

                                    Intent barcode = new Intent(mContext,ScanActivity.class);

                                    barcode.putExtra(ActivityRadioButton.POSITION, String.valueOf(position));
                                    barcode.putExtra(ActivityRadioButton.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                    ((ActivityPertanyaanChild)mContext).startActivityForResult(barcode, 1);
                                    break;
                                case "Choice" :

                                    if (singleModelProduct.isMultiple()) {
                                        ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                        ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                        Intent multiple =new Intent(mContext,ActivityCheckBox.class);
                                        multiple.putExtra(ActivityCheckBox.POSITION, String.valueOf(position));
                                        multiple.putExtra(ActivityCheckBox.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                        multiple.putExtra(ActivityCheckBox.MAX, singleModelProduct.getMaximum());
                                        ((ActivityPertanyaanChild)mContext).startActivityForResult(multiple, 1);
                                    }
                                    else {
                                        ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                        ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                        Intent choice = new Intent(mContext,ActivityRadioButton.class);
                                        choice.putExtra(ActivityRadioButton.POSITION, String.valueOf(position));
                                        choice.putExtra(ActivityRadioButton.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                        choice.putExtra(ActivityCheckBox.MAX, singleModelProduct.getMaximum());
                                        ((ActivityPertanyaanChild)mContext).startActivityForResult(choice, 1);
                                    }
                                    break;
                                case "Text" :

                                    ((ActivityPertanyaanChild)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaanChild)mContext).JENIS = "TEXT";

                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaanChild)mContext).TIPE_TULISAN= singleModelProduct.getTipeTulisan();
                                    if (singleModelProduct.getTipeTulisan().equals("lower")) {
                                        Open(((ActivityPertanyaanChild)mContext).tvLower,singleModelProduct.getJawaban());
                                    }
                                    else {
                                        Open(((ActivityPertanyaanChild)mContext).tvJawaban,singleModelProduct.getJawaban());
                                    }
                                    break;
                                case "Email" :

                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                    Open(((ActivityPertanyaanChild)mContext).tvEmail,singleModelProduct.getJawaban());
                                    break;
                                case "Note" :
                                    ((ActivityPertanyaanChild)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaanChild)mContext).JENIS = "TEXT";

                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                    Open(((ActivityPertanyaanChild)mContext).tvNote,singleModelProduct.getJawaban());
                                    break;
                                case "Integer" :

                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaanChild)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaanChild)mContext).JENIS = "NUMBER";
                                    Open(((ActivityPertanyaanChild)mContext).tvNumber,singleModelProduct.getJawaban());
                                    break;
                                case "Phone" :

                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaanChild)mContext).tvNumber.setHint("Masukkan Nomor Telepon");
                                    ((ActivityPertanyaanChild)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaanChild)mContext).JENIS = "NUMBER";
                                    Open(((ActivityPertanyaanChild)mContext).tvNumber,singleModelProduct.getJawaban());
                                    break;
                                case "Number" :
                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaanChild)mContext).tvNumber.setHint("Masukkan Jawaban Anda");
                                    ((ActivityPertanyaanChild)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaanChild)mContext).JENIS = "NUMBER";
                                    Open(((ActivityPertanyaanChild)mContext).tvNumber,singleModelProduct.getJawaban());
                                    break;
                                case "Float" :
                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaanChild)mContext).tvFloat.setHint("Masukkan Jawaban Anda");
                                    ((ActivityPertanyaanChild)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaanChild)mContext).JENIS = "NUMBER";
                                    Open(((ActivityPertanyaanChild)mContext).tvFloat,singleModelProduct.getJawaban());
                                    break;
                                case "Image" :
                                    mController.setUserProfile(appConfig.TYPE, "CAMERA");
                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaanChild)mContext).GetImage();
                                    break;
                                case "Gallery" :
                                    mController.setUserProfile(appConfig.TYPE, "GALERY");
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).GetImage();
                                case "Date" :

                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaanChild)mContext).OnDate();
                                    break;
                                case "Time" :

                                    ((ActivityPertanyaanChild)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaanChild)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaanChild)mContext).OnTime();
                                    break;
                            }


                        }
                    });

                }
                else {
                    productView.tvDeskripsi.setVisibility(View.GONE);
                    productView.cardViewPertanyaan.setVisibility(View.GONE);
                    productView.cardViewLabel.setVisibility(View.VISIBLE);
                    productView.tvHanyaLabel.setText(singleModelProduct.getPertanyaan());
                }
            }
            else {
                productView.Layout_hide();
            }


        }
    }


    public void Open (EditText textView, String jawaban) {

        if (((ActivityPertanyaanChild)mContext).JENIS.equals("NUMBER")) {

        }
        else {
            if (((ActivityPertanyaanChild)mContext).TIPE_TULISAN.equals("free")) {
                Log.d ("TIPE", "FREE");
            }
            else if (((ActivityPertanyaanChild)mContext).TIPE_TULISAN.equals("lower")) {
                Log.d ("TIPE", "LWER");
                //textView.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_FLAG_MULTI_LINE);
            }
            else if (((ActivityPertanyaanChild)mContext).TIPE_TULISAN.equals("upper")) {
                Log.d ("TIPE", "UPPER");
                textView.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
                // textView.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            }
            else {

            }
            ((ActivityPertanyaanChild)mContext).JENIS = "NUMBER";
        }


        ((ActivityPertanyaanChild)mContext).LayoutSubmit.setVisibility(View.GONE);
        ((ActivityPertanyaanChild)mContext).LayoutJawaban.setVisibility(View.VISIBLE);
        ((ActivityPertanyaanChild)mContext).LayoutTransparant .setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);

        if (!jawaban.equals("")) {
            textView.setText(jawaban);
            textView.setSelection(textView.getText().length());
        }

        textView.requestFocus();
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput( textView, InputMethodManager.SHOW_IMPLICIT);
    }




    public static class ModelProductViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPertanyaan;
        public TextView tvIndex;
        public TextView tvJawaban;
        public TextView tvQuestionType;
        public TextView tvMandatory;

        public TextView tvHanyaLabel;
        public CardView cardViewPertanyaan;
        public CardView cardViewLabel;

        public ImageView imgImage;
        public ModelPertanyaan pertanyaan;

        public RecyclerView mRecyclerView;
        public List<ModelPertanyaan> dataList;
        public AdapterPertanyaanChild adapter;
        public String PertanyaanID = "";
        public String POSITION;
        public TextView tvDeskripsi;


        // public LinearLayout LayoutParent;

        public LinearLayout ifHidden;
        public LinearLayout ifShow;
        private View mView;

        private final LinearLayout layout;
        final LinearLayout.LayoutParams params;
        private int tinggi;

        private void Layout_hide() {
            params.height = 0;
            //itemView.setLayoutParams(params); //This One.
            layout.setLayoutParams(params);   //Or This one.
        }

        private void Layout_Show() {
            params.height = tinggi;
            //itemView.setLayoutParams(params); //This One.
            layout.setLayoutParams(params);   //Or This one.
        }

        public ModelProductViewHolder(Context context, View v) {
            super(v);


            layout =(LinearLayout)itemView.findViewById(R.id.show_item_layout);
            params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);


            tinggi = params.height;


            mView = v;
            tvPertanyaan = (TextView) v.findViewById(R.id.tvPertanyaan);
            tvJawaban = (TextView) v.findViewById(R.id.tvJawaban);
            tvDeskripsi = (TextView) v.findViewById(R.id.tvDeskripsi);


            tvIndex = (TextView) v.findViewById(R.id.tvIndex);
            tvQuestionType = (TextView) v.findViewById(R.id.tvQuestionType);
            imgImage = (ImageView) v.findViewById(R.id.imgImage);
            tvMandatory = (TextView) v.findViewById(R.id.tvMandatory);
            mRecyclerView       = (RecyclerView) v.findViewById(R.id.child);

            tvHanyaLabel        = (TextView) v.findViewById(R.id.tvHanyaLabel);
            cardViewLabel       = (CardView) v.findViewById(R.id.CardViewLabel);
            cardViewPertanyaan  = (CardView) v.findViewById(R.id.CardViewPertanyaan);


            ifHidden            = (LinearLayout) v.findViewById(R.id.ifHidden);
            ifShow              = (LinearLayout) v.findViewById(R.id.ifShow);
        }
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {

        public ProgressViewHolder(View v) {
            super(v);
            //progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
}
