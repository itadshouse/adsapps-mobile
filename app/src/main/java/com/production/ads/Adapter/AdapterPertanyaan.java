package com.production.ads.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.production.ads.Activity.ActivityCheckBox;
import com.production.ads.Activity.ActivityMap;
import com.production.ads.Activity.ActivityPertanyaan;
import com.production.ads.Activity.ActivityPertanyaanChild;
import com.production.ads.Activity.ActivityRadioButton;
import com.production.ads.Activity.ScanActivity;
import com.production.ads.App.appConfig;
import com.production.ads.App.appController;
import com.production.ads.Model.ModelPertanyaan;
import com.production.ads.R;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class AdapterPertanyaan extends RecyclerView.Adapter {

    private List<ModelPertanyaan> model;
    private static Context mContext;
    private static appController mController;
    public ModelProductViewHolder productView;

    public AdapterPertanyaan(Context context, List<ModelPertanyaan> model, RecyclerView recyclerView, View v) {
        this.model = model;
        mContext = context;
        mController = appController.getInstance();

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_pertanyaan, parent, false);
        vh = new ModelProductViewHolder(mContext, v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ModelProductViewHolder) {

            final ModelPertanyaan singleModelProduct =  model.get(position);
            productView = ((ModelProductViewHolder) holder);
            productView.pertanyaan = singleModelProduct;
            boolean hilang = false;

            if (!hilang) {
                productView.Layout_Show();


                if (singleModelProduct.getQuestion_type().equals("SUBMIT")) {
                    productView.ifShow.setVisibility(View.GONE);
                    productView.ifHidden.setVisibility(View.GONE);
                    productView.LayoutSubmit.setVisibility(View.VISIBLE);
                }
                else if (!singleModelProduct.getQuestion_type().equals("Section")) {

                    productView.ifHidden.setVisibility(View.GONE);
                    productView.ifShow.setVisibility(View.VISIBLE);
                    productView.LayoutSubmit.setVisibility(View.GONE);


                    if (singleModelProduct.getDeskripsi().equals("")) {
                        productView.tvDeskripsi.setVisibility(View.GONE);
                    }
                    else {
                        productView.tvDeskripsi.setVisibility(View.VISIBLE);
                        productView.tvDeskripsi.setText(singleModelProduct.getDeskripsi());
                    }


                    productView.cardViewPertanyaan.setVisibility(View.VISIBLE);
                    productView.cardViewLabel.setVisibility(View.GONE);

                    productView.tvPertanyaan.setText(singleModelProduct.getPertanyaan());
                    productView.tvIndex.setText(singleModelProduct.getPosition());

                    productView.tvJawaban.setText(singleModelProduct.getJawaban());

                    if (singleModelProduct.getQuestion_type().equals("Time")
                            || singleModelProduct.getQuestion_type().equals("Date")
                            || singleModelProduct.getQuestion_type().equals("Image")
                            || singleModelProduct.getQuestion_type().equals("Phone")
                            || singleModelProduct.getQuestion_type().equals("Email")
                            || singleModelProduct.getQuestion_type().equals("Barcode")
                            || singleModelProduct.getQuestion_type().equals("Location")
                            || singleModelProduct.getQuestion_type().equals("Dropdown")
                            || singleModelProduct.getQuestion_type().equals("Group") ||

                            singleModelProduct.getMaximum().toString().equals(null) || singleModelProduct.getMaximum().toString().equals("") || singleModelProduct.getMaximum().toString().equals("0")) {
                        productView.tvQuestionType.setText(singleModelProduct.getQuestion_type());
                    }
                    else {
                        productView.tvQuestionType.setText(singleModelProduct.getQuestion_type() + " - Max : " + singleModelProduct.getMaximum().toString());
                    }

                    productView.tvMandatory.setText(singleModelProduct.getMandatory());
                    productView.imgImage.setImageBitmap(singleModelProduct.getImage());

                    if (!singleModelProduct.getChild().equals("")) {

                    }
                    else {
                        productView.tvJawaban.setVisibility(View.VISIBLE);
                        productView.mRecyclerView.setVisibility(View.GONE);
                    }

                    boolean janganLanjut = false;
                    if (singleModelProduct.getImage() == null) {
                        productView.imgImage.setVisibility(View.GONE);
                        productView.tvJawaban.setVisibility(View.VISIBLE);
                    }
                    else {
                        productView.imgImage.setVisibility(View.VISIBLE);
                        productView.tvJawaban.setVisibility(View.GONE);
                        janganLanjut = true;

                    }

                    if (janganLanjut) {

                    }
                    else {
                        if (!singleModelProduct.getDestination().equals("null")) {
                            Picasso.with(mContext).load("file://"+singleModelProduct.getDestination()).placeholder(mContext.getResources()
                                    .getDrawable(R.drawable.blank))
                                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                                    .into(productView.imgImage);

                            productView.imgImage.setVisibility(View.VISIBLE);
                            productView.tvJawaban.setVisibility(View.GONE);
                        }
                        else {
                            productView.imgImage.setVisibility(View.GONE);
                            productView.tvJawaban.setVisibility(View.VISIBLE);
                        }
                    }

                    productView.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            ((ActivityPertanyaan)mContext).tvJawaban.setVisibility(View.GONE);
                            ((ActivityPertanyaan)mContext).tvEmail.setVisibility(View.GONE);
                            ((ActivityPertanyaan)mContext).tvNumber.setVisibility(View.GONE);
                            ((ActivityPertanyaan)mContext).tvNote.setVisibility(View.GONE);
                            ((ActivityPertanyaan)mContext).tvLower.setVisibility(View.GONE);
                            ((ActivityPertanyaan)mContext).tvFloat.setVisibility(View.GONE);
                            ((ActivityPertanyaan)mContext).tvNote.setText("");
                            ((ActivityPertanyaan)mContext).tvNumber.setText("");
                            ((ActivityPertanyaan)mContext).tvJawaban.setText("");
                            ((ActivityPertanyaan)mContext).tvEmail.setText("");
                            ((ActivityPertanyaan)mContext).tvLower.setText("");
                            ((ActivityPertanyaan)mContext).tvFloat.setText("");
                            switch (singleModelProduct.getQuestion_type()) {
                                case "Dropdown" :
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    Intent intent =new Intent(mContext,ActivityCheckBox.class);
                                    intent.putExtra(ActivityCheckBox.POSITION, String.valueOf(position));
                                    intent.putExtra(ActivityCheckBox.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                    ((ActivityPertanyaan)mContext).startActivityForResult(intent, 1);
                                    break;
                                case "Group" :
                                    Log.d("POSITION", String.valueOf(position));
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    Intent rad = new Intent(mContext, ActivityPertanyaanChild.class);
                                    rad.putExtra(ActivityRadioButton.POSITION, String.valueOf(position));
                                    rad.putExtra("DATA", singleModelProduct.getChild());
                                    rad.putExtra("NAME", singleModelProduct.getPertanyaan());
                                    rad.putExtra("ID_PERTANYAAN", singleModelProduct.getId());
                                    rad.putExtra("ID_FORM", ((ActivityPertanyaan)mContext).intent.getStringExtra(appConfig.FORM_ID));
                                    ((ActivityPertanyaan)mContext).startActivityForResult(rad, 1);
                                    break;
                                case "Radio" :
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    Intent radioButton = new Intent(mContext,ActivityRadioButton.class);
                                    radioButton.putExtra(ActivityRadioButton.POSITION, String.valueOf(position));
                                    radioButton.putExtra(ActivityRadioButton.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                    radioButton.putExtra(ActivityRadioButton.FORM_ID, ((ActivityPertanyaan)mContext).intent.getStringExtra(appConfig.FORM_ID));
                                    ((ActivityPertanyaan)mContext).startActivityForResult(radioButton, 1);
                                    break;
                                case "Location" :
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    Intent map = new Intent(mContext,ActivityMap.class);
                                    map.putExtra(ActivityRadioButton.POSITION, String.valueOf(position));
                                    map.putExtra(ActivityRadioButton.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                    ((ActivityPertanyaan)mContext).startActivityForResult(map, 1);
                                    break;
                                case "Barcode" :
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    Intent barcode = new Intent(mContext,ScanActivity.class);
                                    barcode.putExtra(ActivityRadioButton.POSITION, String.valueOf(position));
                                    barcode.putExtra(ActivityRadioButton.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                    ((ActivityPertanyaan)mContext).startActivityForResult(barcode, 1);
                                    break;
                                case "Choice" :
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    if (singleModelProduct.isMultiple()) {
                                        ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                        ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                        Intent multiple =new Intent(mContext,ActivityCheckBox.class);
                                        multiple.putExtra(ActivityCheckBox.POSITION, String.valueOf(position));
                                        multiple.putExtra(ActivityCheckBox.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                        multiple.putExtra(ActivityCheckBox.MAX, singleModelProduct.getMaximum());
                                        ((ActivityPertanyaan)mContext).startActivityForResult(multiple, 1);
                                    }
                                    else {
                                        ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                        ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                        Intent choice = new Intent(mContext,ActivityRadioButton.class);
                                        choice.putExtra(ActivityRadioButton.POSITION, String.valueOf(position));
                                        choice.putExtra(ActivityRadioButton.ANSWER_GROUP, singleModelProduct.getAnswer_group());
                                        choice.putExtra(ActivityCheckBox.MAX, singleModelProduct.getMaximum());
                                        ((ActivityPertanyaan)mContext).startActivityForResult(choice, 1);
                                    }
                                    break;
                                case "Text" :

                                    ((ActivityPertanyaan)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaan)mContext).JENIS = "TEXT";
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).TIPE_TULISAN= singleModelProduct.getTipeTulisan();
                                    if (singleModelProduct.getTipeTulisan().equals("lower")) {
                                        Open(((ActivityPertanyaan) mContext).tvLower,singleModelProduct.getJawaban());
                                    }
                                    else {
                                        Open(((ActivityPertanyaan) mContext).tvJawaban,singleModelProduct.getJawaban());
                                    }
                                    break;
                                case "Email" :
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    Open(((ActivityPertanyaan) mContext).tvEmail,singleModelProduct.getJawaban());
                                    break;
                                case "Note" :
                                    ((ActivityPertanyaan)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaan)mContext).JENIS = "TEXT";
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    Open(((ActivityPertanyaan) mContext).tvNote,singleModelProduct.getJawaban());
                                    break;
                                case "Integer" :
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaan)mContext).JENIS = "NUMBER";
                                    Open(((ActivityPertanyaan) mContext).tvNumber,singleModelProduct.getJawaban());
                                    break;
                                case "Phone" :
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan) mContext).tvNumber.setHint("Masukkan Nomor Telepon");
                                    ((ActivityPertanyaan)mContext).MAX = "2000";
                                    ((ActivityPertanyaan)mContext).JENIS = "NUMBER";
                                    Open(((ActivityPertanyaan) mContext).tvNumber,singleModelProduct.getJawaban());
                                    break;
                                case "Number" :
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan) mContext).tvNumber.setHint("Masukkan Jawaban Anda");
                                    ((ActivityPertanyaan)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaan)mContext).JENIS = "NUMBER";
                                    Open(((ActivityPertanyaan) mContext).tvNumber,singleModelProduct.getJawaban());
                                    break;
                                case "Float" :
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan) mContext).tvFloat.setHint("Masukkan Jawaban Anda");
                                    ((ActivityPertanyaan)mContext).MAX = singleModelProduct.getMaximum();
                                    ((ActivityPertanyaan)mContext).JENIS = "NUMBER";
                                    Open(((ActivityPertanyaan) mContext).tvFloat,singleModelProduct.getJawaban());
                                    break;
                                case "Image" :
                                    Log.d("IMAGE_SIZE", "HALLLOOOOOOOOOOOOOOO 2");
                                    mController.setUserProfile(appConfig.TYPE, "CAMERA");
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).GetImage();
                                    break;
                                case "Gallery" :
                                    mController.setUserProfile(appConfig.TYPE, "GALERY");
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).GetImage();
                                    break;
                                case "Date" :
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).OnDate();
                                    break;
                                case "Time" :
                                    ((ActivityPertanyaan)mContext).isChild = false;
                                    ((ActivityPertanyaan)mContext).POSITION = String.valueOf(position);
                                    ((ActivityPertanyaan)mContext).PertanyaanID = singleModelProduct.getId();
                                    ((ActivityPertanyaan)mContext).OnTime();
                                    break;
                            }


                        }
                    });

                }
                else {
                    productView.ifShow.setVisibility(View.VISIBLE);
                    productView.ifHidden.setVisibility(View.GONE);
                    productView.LayoutSubmit.setVisibility(View.GONE);

                    productView.tvDeskripsi.setVisibility(View.GONE);
                    productView.cardViewPertanyaan.setVisibility(View.GONE);
                    productView.cardViewLabel.setVisibility(View.VISIBLE);
                    productView.tvHanyaLabel.setText(singleModelProduct.getPertanyaan());
                }
                singleModelProduct.setManda(true);
            }
            else {
                productView.Layout_hide();
                singleModelProduct.setManda(false);
            }


        }
    }

    public void Open (EditText textView, String jawaban) {

        if (((ActivityPertanyaan)mContext).JENIS.equals("NUMBER")) {

        }
        else {
            if (((ActivityPertanyaan)mContext).TIPE_TULISAN.equals("free")) {
                Log.d ("TIPE", "FREE");
            }
            else if (((ActivityPertanyaan)mContext).TIPE_TULISAN.equals("lower")) {
                Log.d ("TIPE", "LWER");
                //textView.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_FLAG_MULTI_LINE);
            }
            else if (((ActivityPertanyaan)mContext).TIPE_TULISAN.equals("upper")) {
                Log.d ("TIPE", "UPPER");
                textView.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
               // textView.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            }
            else {

            }
            ((ActivityPertanyaan)mContext).JENIS = "NUMBER";
        }


        //((ActivityPertanyaan)mContext).LayoutSubmit.setVisibility(View.GONE);
        ((ActivityPertanyaan)mContext).LayoutJawaban.setVisibility(View.VISIBLE);
        ((ActivityPertanyaan)mContext).LayoutTransparant .setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);

        if (!jawaban.equals("")) {
            textView.setText(jawaban);
            textView.setSelection(textView.getText().length());
        }

        textView.requestFocus();
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput( textView, InputMethodManager.SHOW_IMPLICIT);
    }




    public static class ModelProductViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPertanyaan;
        public TextView tvIndex;
        public TextView tvJawaban;
        public TextView tvQuestionType;
        public TextView tvMandatory;

        public TextView tvHanyaLabel;
        public TextView tvDeskripsi;
        public CardView cardViewPertanyaan;
        public CardView cardViewLabel;

        public ImageView imgImage;
        public ModelPertanyaan pertanyaan;

        public RecyclerView mRecyclerView;
        public List<ModelPertanyaan> dataList;
        public AdapterPertanyaanChild adapter;
        public String PertanyaanID = "";
        public String POSITION;



        // public LinearLayout LayoutParent;

        public LinearLayout ifHidden;
        public LinearLayout ifShow;
        private View mView;

        private final LinearLayout layout;
        final LinearLayout.LayoutParams params;
        private int tinggi;

        private RelativeLayout LayoutSubmit;
        private Button btnSubmit;

        private void Layout_hide() {
            params.height = 0;
            //itemView.setLayoutParams(params); //This One.
            layout.setLayoutParams(params);   //Or This one.
        }

        private void Layout_Show() {
            params.height = tinggi;
            //itemView.setLayoutParams(params); //This One.
            layout.setLayoutParams(params);   //Or This one.
        }

        public ModelProductViewHolder(Context context, View v) {
            super(v);


            layout =(LinearLayout)itemView.findViewById(R.id.show_item_layout);
            params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);


            tinggi = params.height;


            mView = v;
            tvPertanyaan = (TextView) v.findViewById(R.id.tvPertanyaan);
            tvJawaban = (TextView) v.findViewById(R.id.tvJawaban);
            tvDeskripsi = (TextView) v.findViewById(R.id.tvDeskripsi);



            tvIndex                 = (TextView) v.findViewById(R.id.tvIndex);
            tvQuestionType          = (TextView) v.findViewById(R.id.tvQuestionType);
            imgImage                = (ImageView) v.findViewById(R.id.imgImage);
            tvMandatory             = (TextView) v.findViewById(R.id.tvMandatory);
            mRecyclerView           = (RecyclerView) v.findViewById(R.id.child);

            tvHanyaLabel            = (TextView) v.findViewById(R.id.tvHanyaLabel);
            cardViewLabel           = (CardView) v.findViewById(R.id.CardViewLabel);
            cardViewPertanyaan      = (CardView) v.findViewById(R.id.CardViewPertanyaan);


            ifHidden            = (LinearLayout) v.findViewById(R.id.ifHidden);
            ifShow              = (LinearLayout) v.findViewById(R.id.ifShow);

            LayoutSubmit        = v.findViewById(R.id.LayoutSubmit);
            btnSubmit           = v.findViewById(R.id.btnSubmit);

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ActivityPertanyaan) mContext).GoSubmit(btnSubmit);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {

        public ProgressViewHolder(View v) {
            super(v);
            //progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
}
