package com.production.ads.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.production.ads.Activity.ActivityPertanyaan;
import com.production.ads.Activity.ActivitySurvey;
import com.production.ads.App.appConfig;
import com.production.ads.App.appController;
import com.production.ads.Model.ModelSurvey;
import com.production.ads.R;
import java.util.List;

public class AdapterSurvey extends RecyclerView.Adapter {


    private List<ModelSurvey> model;


    private static Context mContext;
    private static appController mController;

    public AdapterSurvey(Context context, List<ModelSurvey> model, RecyclerView recyclerView, View v) {
        this.model = model;
        mContext = context;
        mController = appController.getInstance();

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

        }
    }


    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;

        View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.row_survey, parent, false);
        vh = new ModelProductViewHolder(mContext, v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ModelProductViewHolder) {

            final ModelSurvey singleModelProduct = model.get(position);
            final ModelProductViewHolder productView = ((ModelProductViewHolder) holder);
            productView.survey = singleModelProduct;

            productView.tvNamaSurvey.setText(singleModelProduct.getNamaSurvey());
            productView.tvJumlahPertanyaan.setText(singleModelProduct.getKeterangan() +  ", Draft : " + singleModelProduct.getDraft() +
                    " , Tersimpan : " + singleModelProduct.getDataTersimpan());

            if (singleModelProduct.getInfo().equals("Null")) {
                productView.info.setVisibility(View.GONE);
                productView.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent integer = new Intent(mContext,ActivityPertanyaan.class);
                        integer.putExtra(appConfig.FORM_ID, singleModelProduct.getId());
                        integer.putExtra("FORM_NAME", singleModelProduct.getNamaSurvey());
                        integer.putExtra("DETAIL", singleModelProduct.getDetail());
                        ((ActivitySurvey)mContext).startActivityForResult(integer, 1);
                    }
                });
            }
            else {
                productView.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent integer = new Intent(mContext,ActivityPertanyaan.class);
                        integer.putExtra(appConfig.FORM_ID, singleModelProduct.getId());
                        integer.putExtra("FORM_NAME", singleModelProduct.getNamaSurvey());
                        integer.putExtra("DETAIL", singleModelProduct.getDetail());
                        ((ActivitySurvey)mContext).startActivityForResult(integer, 1);
                    }
                });
            }
        }
    }

    public static class ModelProductViewHolder extends RecyclerView.ViewHolder {
        public TextView tvJumlahPertanyaan;
        public TextView tvNamaSurvey;
        public ModelSurvey survey;

        private View mView;
        public TextView info;

        public ModelProductViewHolder(Context context, View v) {
            super(v);
            mView = v;
            tvJumlahPertanyaan      = v.findViewById(R.id.tvJumlahPertanyaan);
            tvNamaSurvey            = v.findViewById(R.id.tvNamaSurvey);
            info                    = v.findViewById(R.id.info);
        }
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {

        public ProgressViewHolder(View v) {
            super(v);
            //progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
}
