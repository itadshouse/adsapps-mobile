package com.production.ads.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.production.ads.Activity.ActivityOffline;
import com.production.ads.App.appConfig;
import com.production.ads.App.appController;
import com.production.ads.ModelRealm.ModelSurveyR;
import com.production.ads.Offline.ModelOffline;
import com.production.ads.Offline.ModelOfflineDetail;
import com.production.ads.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public class AdapterOffline extends RecyclerView.Adapter {

    private List<ModelOffline> model;
    private static Context mContext;
    private static appController mController;

    public ModelProductViewHolder productView;
    private ProgressDialog progress;
    private String status = "";
    private String saved_form = "";
    private Realm realm;
    public String[] pertanyaanID;
    public String[] jawaban;
    private int index = 0;

    private String FormID         = "";
    private String UserID         = "";
    private String TimeStamp      = "";
    private String Latitude       = "";
    private String Longtitude     = "";
    private String IMEI           = "";
    private String IMEI_MD5       = "";

    private Call<ResponseBody> result;


    public interface Send {
        @FormUrlEncoded
        @POST("user/form/save")
        Call<ResponseBody> postMessage(@FieldMap HashMap<String, String> params);
    }

    private void Send (final String ID_CREATED_AT){


        index = 0;
        RealmResults<ModelOfflineDetail> rs = realm.where(ModelOfflineDetail.class).
                equalTo("CreatedAt", ID_CREATED_AT).
                findAll();
        realm.beginTransaction();
        for (int j = 0; j < rs.size(); j++) {
            pertanyaanID[j] = rs.get(j).getPertanyaanID();
            jawaban[j]      = rs.get(j).getJawaban();

            index++;
        }
        realm.commitTransaction();

        FormID         = "";
        UserID         = "";
        TimeStamp      = "";
        Latitude       = "";
        Longtitude     = "";
        IMEI           = "";
        IMEI_MD5       = "";


        RealmResults<ModelOffline> r = realm.where(ModelOffline.class).
                equalTo("CreatedAt", ID_CREATED_AT).
                findAll();
        realm.beginTransaction();
        for (int j = 0; j < r.size(); j++) {
            FormID         = r.get(j).getFormID();
            UserID         = r.get(j).getUserID();
            TimeStamp      = r.get(j).getTimeStamp();
            Latitude       = r.get(j).getLatitude();
            Longtitude     = r.get(j).getLongtitude();
            IMEI           = r.get(j).getIMEI();
            IMEI_MD5       = r.get(j).getIMEI_MD5();
        }
        realm.commitTransaction();




        progress = new ProgressDialog(mContext);
        progress.setMessage("Sedang Mengirim ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.show();

        HashMap<String, String> params = new HashMap<>();
        params.put("userID", UserID);
        params.put("formID", FormID);
        params.put("timeStamp", TimeStamp);
        params.put("latitude", Latitude);
        params.put("longtitude", Longtitude);
        params.put("IMEI", IMEI);
        params.put("IMEI_MD5", IMEI_MD5);

        for (int j = 0; j < index; j++) {
            params.put("pertanyaanId[" + String.valueOf(j) + "]", pertanyaanID[j]);
            params.put("jawaban[" + String.valueOf(j) + "]", jawaban[j]);


            Log.d("pertanyaanId[" + String.valueOf(j) + "]", pertanyaanID[j]);
            Log.d("jawaban[" + String.valueOf(j) + "]", jawaban[j]);

        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(appConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mController.client)
                .build();

        Send apiService = retrofit.create(Send.class);
        result = apiService.postMessage(params);
        result.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                try {
                    progress.dismiss();
                    new AlertDialog.Builder(mContext)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Submit")
                            .setMessage("Data Gagal Terkirim")
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }
                catch (Exception e) {

                }
            }

            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> r) {
                progress.dismiss();
                try {
                    status = "";
                    saved_form = "";
                    String response = r.body().string();
                    JSONObject jData = new  JSONObject(response);
                    status = jData.getString("status");
                    saved_form = jData.getString("saved_form");

                    mController.setUserProfile(appConfig.LAST_FORM, FormID);
                    mController.setUserProfile(appConfig.LAST_FORM_JUMLAH, saved_form);

                    RealmResults<ModelSurveyR> s =
                            realm.where(ModelSurveyR.class).equalTo("id", FormID).findAll();
                    ModelSurveyR rR = new ModelSurveyR();
                    realm.beginTransaction();
                    rR.setId(s.get(0).getId());
                    rR.setNamaSurvey(s.get(0).getNamaSurvey());
                    rR.setJumlahPertanyaan(s.get(0).getJumlahPertanyaan());
                    rR.setKeterangan(s.get(0).getKeterangan());
                    rR.setInfo(s.get(0).getInfo());
                    rR.setDetail(s.get(0).getDetail());
                    rR.setDraft(s.get(0).getDraft());
                    rR.setDataTersimpan(s.get(0).getDataTersimpan());
                    rR.setKeterangan(s.get(0).getKeterangan());
                    rR.setDataTersimpan(s.get(0).getDataTersimpan());


                    if (status.equals("Anda tidak memiliki akses..")) {
                        rR.setHilang("True");
                    }
                    else {
                        rR.setHilang(s.get(0).getHilang());
                    }

                    realm.copyToRealmOrUpdate(rR);
                    realm.commitTransaction();

                    RealmResults<ModelOffline> rr = realm.where(ModelOffline.class).
                            equalTo("CreatedAt", ID_CREATED_AT).
                            findAll();
                    realm.beginTransaction();
                    rr.clear();
                    realm.commitTransaction();


                    RealmResults<ModelOfflineDetail> rs = realm.where(ModelOfflineDetail.class).
                            equalTo("CreatedAt", ID_CREATED_AT).
                            findAll();
                    realm.beginTransaction();
                    rs.clear();
                    realm.commitTransaction();


                    new AlertDialog.Builder(mContext)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Submit Keterangan")
                            .setMessage(status)
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    ((ActivityOffline)mContext).LoadData();
                                }
                            })
                            .show();
                }
                catch (Exception e) {
                    try {
                        progress.dismiss();
                        new AlertDialog.Builder(mContext)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle("Submit")
                                .setMessage("Data Gagal Terkirim")
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .show();
                    }
                    catch (Exception de) {

                    }
                }
            }
        });
    }




    public AdapterOffline(Context context, List<ModelOffline> model, RecyclerView recyclerView, View v) {
        this.model = model;
        mContext = context;
        mController = appController.getInstance();


        pertanyaanID    = new String[500];
        jawaban         = new String[500];


        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(mContext).build();

        try {
            realm = Realm.getInstance(realmConfiguration);
        } catch (RealmMigrationNeededException e){
            try {
                Realm.deleteRealm(realmConfiguration);
                realm = Realm.getInstance(realmConfiguration);
            } catch (Exception ex){
                throw ex;
            }
        }



        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_offline, parent, false);
        vh = new ModelProductViewHolder(mContext, v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ModelProductViewHolder) {

            final ModelOffline singleModelProduct =  model.get(position);
            productView = ((ModelProductViewHolder) holder);
            productView.offline = singleModelProduct;
            productView.created_at.setText("Created At : " + singleModelProduct.getCreatedAt());
            productView.tvNama.setText(singleModelProduct.getFormName());

            productView.kirim.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Send(singleModelProduct.getCreatedAt());
                }
            });


        }
    }



    public static class ModelProductViewHolder extends RecyclerView.ViewHolder {

        public TextView tvNama;
        public TextView created_at;
        public Button kirim;


        public ModelOffline offline;
        private View mView;

        public ModelProductViewHolder(Context context, View v) {
            super(v);
            mView = v;

            tvNama      = v.findViewById(R.id.tvNama);
            created_at  = v.findViewById(R.id.created_at);
            kirim       = v.findViewById(R.id.kirim);

        }
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {

        public ProgressViewHolder(View v) {
            super(v);
            //progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
}
