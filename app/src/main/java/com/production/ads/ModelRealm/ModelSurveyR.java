package com.production.ads.ModelRealm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class ModelSurveyR extends RealmObject {

    @PrimaryKey
    private String id;
    private String keterangan;
    private String jumlahPertanyaan;
    private String namaSurvey;
    private String info;
    private String detail;
    private String dataTersimpan;
    private String draft;
    private String hilang;

    public String getHilang() {
        return hilang;
    }

    public void setHilang(String hilang) {
        this.hilang = hilang;
    }

    public String getDraft() {
        return draft;
    }

    public void setDraft(String draft) {
        this.draft = draft;
    }

    public String getDataTersimpan() {
        return dataTersimpan;
    }

    public void setDataTersimpan(String dataTersimpan) {
        this.dataTersimpan = dataTersimpan;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getJumlahPertanyaan() {
        return jumlahPertanyaan;
    }

    public void setJumlahPertanyaan(String jumlahPertanyaan) {
        this.jumlahPertanyaan = jumlahPertanyaan;
    }

    public String getNamaSurvey() {
        return namaSurvey;
    }

    public void setNamaSurvey(String namaSurvey) {
        this.namaSurvey = namaSurvey;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
