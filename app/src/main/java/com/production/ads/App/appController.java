package com.production.ads.App;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;
import com.crashlytics.android.Crashlytics;
import com.production.ads.R;
import io.fabric.sdk.android.Fabric;
import java.net.CookieHandler;
import java.net.CookieManager;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class appController extends MultiDexApplication {
    public static final String TAG = appController.class.getSimpleName();


    private static appController mInstance;
    public OkHttpClient client;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        CookieHandler cookieHandler = new CookieManager();
        client = new OkHttpClient.Builder().addNetworkInterceptor(interceptor)
                .cookieJar(new JavaNetCookieJar(cookieHandler))
                .build();


        mInstance = this;






        Fabric.with(this,new Crashlytics());
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/TitilliumWeb-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

    }

    public static synchronized appController getInstance() {
        return mInstance;
    }

    public SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(context.getApplicationContext().getClass().getSimpleName(),
                Context.MODE_PRIVATE);
    }


    public void setUserProfile(String key, String value) {
        SharedPreferences.Editor edit = getPreferences(getApplicationContext()).edit();
        edit.putString(key, value);
        edit.apply();
    }

    public String getUserProfile(String key) {
        return getPreferences(getApplicationContext()).getString(key, "");
    }

    public void setLogin(String EMAIL) {
        SharedPreferences.Editor edit = getPreferences(getApplicationContext()).edit();
        edit.putString(appConfig.EMAIL, EMAIL);
        edit.apply();
    }

    public boolean isLoggedIn() {
        return !getUserProfile(appConfig.EMAIL).equals("");
    }

    public void DeleteALL () {
        SharedPreferences.Editor edit = getPreferences(getApplicationContext()).edit();
        edit.clear();
        edit.commit();
        edit.apply();
    }


}
