package com.production.ads.Model;

import android.graphics.Bitmap;

public class ModelPertanyaan {

    private String jawaban;
    private Bitmap image;
    private String answer_group;
    private String question_type;
    private String pertanyaan = null;
    private String id;
    private String nomor;
    private String urutan;
    private String mandatory;
    private String child;
    private boolean isMultiple;
    private String kondisi;

    private String detail_kondisi_id;
    private String detail_kondisi_notasi;
    private String detail_kondisi_nilai;

    private String detail_kondisi_id_2;
    private String detail_kondisi_notas_2;
    private String detail_kondisi_nilai_2;

    private String position;
    private String destination;

    private String maximum;

    private String tipeTulisan;
    private String deskripsi;

    private boolean manda;

    public boolean isManda() {
        return manda;
    }

    public void setManda(boolean manda) {
        this.manda = manda;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTipeTulisan() {
        return tipeTulisan;
    }

    public void setTipeTulisan(String tipeTulisan) {
        this.tipeTulisan = tipeTulisan;
    }

    public String getMaximum() {
        return maximum;
    }

    public void setMaximum(String maximum) {
        this.maximum = maximum;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDetail_kondisi_nilai() {
        return detail_kondisi_nilai;
    }

    public void setDetail_kondisi_nilai(String detail_kondisi_nilai) {
        this.detail_kondisi_nilai = detail_kondisi_nilai;
    }

    public String getDetail_kondisi_nilai_2() {
        return detail_kondisi_nilai_2;
    }

    public void setDetail_kondisi_nilai_2(String detail_kondisi_nilai_2) {
        this.detail_kondisi_nilai_2 = detail_kondisi_nilai_2;
    }

    public String getDetail_kondisi_id_2() {
        return detail_kondisi_id_2;
    }

    public void setDetail_kondisi_id_2(String detail_kondisi_id_2) {
        this.detail_kondisi_id_2 = detail_kondisi_id_2;
    }

    public String getDetail_kondisi_notas_2() {
        return detail_kondisi_notas_2;
    }

    public void setDetail_kondisi_notas_2(String detail_kondisi_notas_2) {
        this.detail_kondisi_notas_2 = detail_kondisi_notas_2;
    }

    public String getDetail_kondisi_id() {
        return detail_kondisi_id;
    }

    public void setDetail_kondisi_id(String detail_kondisi_id) {
        this.detail_kondisi_id = detail_kondisi_id;
    }

    public String getDetail_kondisi_notasi() {
        return detail_kondisi_notasi;
    }

    public void setDetail_kondisi_notasi(String detail_kondisi_notasi) {
        this.detail_kondisi_notasi = detail_kondisi_notasi;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public boolean isMultiple() {
        return isMultiple;
    }

    public void setMultiple(boolean multiple) {
        isMultiple = multiple;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getUrutan() {
        return urutan;
    }

    public void setUrutan(String urutan) {
        this.urutan = urutan;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public ModelPertanyaan() {

    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    public String getAnswer_group() {
        return answer_group;
    }

    public void setAnswer_group(String answer_group) {
        this.answer_group = answer_group;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
